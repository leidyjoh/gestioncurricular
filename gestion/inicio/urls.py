# -*- encoding: utf-8 -*-

from django.urls import path
from django.contrib import admin
from .views import Login, Logout, Iniciar


app_name = 'inicio'


urlpatterns = [

	#Para el inicio y cierre de sesion
	path('', Iniciar.as_view(), name='inicio'),
	#path('', Login.as_view(), name='login'),
	path('inicio/login/', Login.as_view(), name='login'),
	path('inicio/logout/', Logout.as_view(), name='logout'),
]