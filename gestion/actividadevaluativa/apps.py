from django.apps import AppConfig


class ActividadevaluativaConfig(AppConfig):
    name = 'actividadevaluativa'
