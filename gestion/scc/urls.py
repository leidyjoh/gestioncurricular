 # -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import MesoCreateView

app_name = 'facultad'

urlpatterns = [

	path('meso/crear/', MesoCreateView.as_view(), name='crear'),
	#path('scc/listar/', FacultadListView.as_view(), name='listar'),
]