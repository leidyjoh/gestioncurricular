 # -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import PerfilCreateView, PerfilUpdateView, PerfilDeleteView, ProgramaEducativoCreateView, ProgramaEducativoUpdateView, ProgramaEducativoDeleteView
from .views import PerfilListView, AmbitosListView, PdfPerfilCreateView, ProgramaEducativoListView

app_name = 'perfil'

urlpatterns = [

	path('perfil/crear/', PerfilCreateView.as_view(), name='crear'),
	path('perfil/listar/', PerfilListView.as_view(), name='listar'),
	path('perfil/listar/ambitos', AmbitosListView.as_view(), name='listar-ambitos'),
	path('perfil/actualizar/<int:pk>', PerfilUpdateView.as_view(), name='actualizar'),
	path('perfil/eliminar/<int:pk>', PerfilDeleteView.as_view(), name='eliminar'),
	path('perfil/PDF/(<int:pk>)', PdfPerfilCreateView.as_view(), name='perfilpdf'),
	path('perfil/programaeducativo/crear', ProgramaEducativoCreateView.as_view(), name='crearpep'),
	path('perfil/programaeducativo/listar/', ProgramaEducativoListView.as_view(), name='listarpep'),
	path('perfil/programaeducativo/actualizar/<int:pk>', ProgramaEducativoUpdateView.as_view(), name='actualizarpep'),
	path('perfil/programaeducativo/eliminar/<int:pk>', ProgramaEducativoDeleteView.as_view(), name='eliminarpep'),

]