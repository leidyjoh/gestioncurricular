# -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import ActividadAprendizajeCreateView, ActividadAprendizajeUpdateView
from .views import ActividadAprendizajeListView

app_name = 'actividadaprendizaje'

urlpatterns = [

	path('actividadaprendizaje/crear/', ActividadAprendizajeCreateView.as_view(), name='crear'),
	path('actividadaprendizaje/listar/', ActividadAprendizajeListView.as_view(), name='listar'),
	path('actividadaprendizaje/actualizar/<int:pk>', ActividadAprendizajeUpdateView.as_view(), name='actualizar'),
]