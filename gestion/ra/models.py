
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from componente.models import Componente
from scc.models import SCC
from matriztributacion.models import CursosTributacionMatriz
from perfil.models import Competencias
from sccfacultad.models import SCCFacultad
from mallacurricular.models import CursosMalla
from programa.models import Programa
from facultad.models import NivelAprendizaje


#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class ResultadoAprendizaje(models.Model):
	"""Permite definir un listado de SCC"""

	#Número del ra
	numero = models.CharField(max_length=200, verbose_name= ('Identificador'))
	#Accion del componente
	accion = models.CharField(max_length=200, verbose_name= ('Acción'), null=True, blank=True, default="")
	#Contenido del componente
	contenido = models.CharField(max_length=200, null=True, blank=True, default="")
	#Contexto del componente
	contexto = models.CharField(max_length=200, null=True, blank=True, default="")
	#Finalidad del componente
	finalidad = models.CharField(max_length=200, null=True, blank=True, default="")
	#Descripción del ra
	descripcion = models.TextField(verbose_name= ('Descripción'))
	#Nivel de aprendizaje del ra
	nivelAprendizaje = models.ForeignKey(NivelAprendizaje, on_delete=models.CASCADE, verbose_name= ('Nivel de aprendizaje'), null=True, blank=True,)
	#Programa al que pertenece
	programa = models.ForeignKey(Programa, on_delete=models.CASCADE, null=True, blank=True)
	#Programa al que pertenece
	scc = models.ForeignKey(CursosTributacionMatriz, on_delete=models.CASCADE, null=True, blank=True, verbose_name= ('SCCs asignado'))
	#Programa al que pertenece
	curso = models.ForeignKey(CursosMalla, on_delete=models.CASCADE, null=True, blank=True, verbose_name= ('Cursos asignados'))
	#Programa al que pertenece
	porcentaje = models.IntegerField(verbose_name= ('Porcentaje de evaluación'), null=True, blank=True,)
	#Actividad de evaluación
	posiblesActividades = models.TextField(max_length=58000, verbose_name='Posibles actividades')
	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['numero']
		verbose_name_plural = "Resultados de Aprendizajes"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return str(self.descripcion)

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.descripcion)
