# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from sccfacultad.models import SCCFacultad

#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class ComponenteFacultad(models.Model):
	"""Permite definir un listado de SCC"""

	#Número del componente
	numero = models.FloatField(verbose_name= ('Número'))
	#Descripción del componente
	descripcion = models.TextField(verbose_name= ('Descripción'))
	#Scc al que pertenece
	scc = models.ForeignKey(SCCFacultad, on_delete=models.CASCADE, verbose_name= ('SCC Facultad'))


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['numero']
		verbose_name_plural = "Componentes Facultad"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return str(self.numero) + " " + self.descripcion

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.numero) + " " + self.descripcion
