# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import ComponenteFacultad


"""Permite administrar la visualización de los datos de Facultad en la base de datos del
	sitio de administración"""
class AdminComponenteFacultad(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Facultad que se mostrarán en el sitio de administración"""
	list_display = ('id', 'numero', 'descripcion', 'scc')
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Facultad en el sitio 
	de administración"""
	search_fields = ('numero', 'scc')

#Permite registrar las clases Facultad y AdminFacultad
admin.site.register(ComponenteFacultad, AdminComponenteFacultad)