# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from .models import ActividadAprendizaje
from inicio.mixins import LoginRequiredMixin
from django.shortcuts import render

"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class ActividadAprendizajeListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""
		#ventasproducto = ventas.values("articulo").annotate(cuantos=Sum('totalProducto')).order_by(Coalesce('cuantos', 'cuantos').desc())

	model = ActividadAprendizaje
	context_object_name = 'actividadesaprendizaje'
	template_name = 'actividadaprendizaje/actividadaprendizaje_list.html'

	#def get_queryset(self):
		#return ComponenteFacultad.values("descripcion").annotate()

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		context = super(ActividadAprendizajeListView,self).get_context_data(**kwargs)
		return context