# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from django.db.models import Count, Sum
from django.db.models.functions import Coalesce
from django.views.generic import ListView, TemplateView
from django.views.generic.detail import DetailView
from programa.models import Programa
from usuario.models import Profesor
from facultad.models import TipoAsignatura
from .models import MallaCurricular, CursosMalla, FiltrosMalla
from django import forms
from curso.models import Curso, Area
from semestre.models import Semestre
from inicio.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.db.models import Count, Sum
from django.db.models.functions import Coalesce
from django.core.serializers.json import DjangoJSONEncoder
import json
from django.db.models.query import QuerySet
from django_group_by import GroupByMixin

"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class ProgramasMallaListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = Programa
	context_object_name = 'listadoprogramas'
	template_name = 'mallacurricular/programas_list.html'
	

	def get_queryset(self):
		#persona = self.kwargs['pk']
		#unidad = user.unidadacademica
		if self.request.user.profesor.cargoAdministrativo == "Director de Escuela":
			return Programa.objects.filter(unidadacademica=self.request.user.profesor.unidadAcademica)
		elif self.request.user.profesor.cargoAdministrativo == "Director de Programa":
			return Programa.objects.filter(unidadacademica=self.request.user.profesor.unidadAcademica)


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class MallaListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = MallaCurricular
	context_object_name = 'mallas'
	template_name = 'mallacurricular/mallacurricular_list.html'

	def get_queryset(self):
		#persona = self.kwargs['pk']
		#unidad = user.unidadacademica
		if self.request.user.profesor.cargoAdministrativo == "Director de Escuela":
			return MallaCurricular.objects.filter(programa__unidadacademica=self.request.user.profesor.unidadAcademica)
		elif self.request.user.profesor.cargoAdministrativo == "Director de Programa":
			return MallaCurricular.objects.filter(programa=self.request.user.profesor.programa)

"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class CursosListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = CursosMalla
	context_object_name = 'cursos'
	template_name = 'mallacurricular/cursos_list.html'

	def get_queryset(self):
		qs = super(CursosListView, self).get_queryset()
		return qs.filter(malla=15)

	def get_context_data(self,**kwargs):
		context = super(CursosListView,self).get_context_data(**kwargs)
		return context

		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		"""context = super(CursosListView,self).get_context_data(**kwargs)
		return context"""


class ReporteCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos de filtros para el reporte Horario por
	carril"""
	class Meta:
		"""Permite determinar el modelo HorarioCarril, con los campos tipo, peaje, carril, 
		fecha, horai y horaf que se muestran, como filtros en el formulario"""
		model = FiltrosMalla
		fields = ['ciclo', 'area', 'asignatura']


class PdfMallaCreateView(LoginRequiredMixin,TemplateView):
	def get(self,request,*args,**kwargs):
		form2 = ReporteCreateForm()
		malla = MallaCurricular.objects.get(id=kwargs['pk'])
		programa = malla.programa
		resolucion = malla.resolucion
		ayuda = malla.ayuda
		escuela = programa.unidadacademica
		semestres = Semestre.objects.filter(programa=programa).order_by('numero')
		cursos = CursosMalla.objects.filter(malla=malla)
		areas = Area.objects.filter(escuela=escuela)
		areasSN=areas.exclude(nombre='Ninguna')
		areas2 = areasSN.values("nombre", "color")
		cursosmalla = cursos.values("curso", "semestre", "id", "curso__creditos", "curso__horas", "curso__horasClase", "curso__tipo__abreviatura", "curso__codigo", "curso__area__color", "curso__ciclo__color", "curso__id", "curso__area__nombre", "curso__ciclo__nombre", "formaciongeneral__nombre")
		coloresCurso = cursos.values("curso__id", "curso__area__color")
		coloresCursoCuantosSon = coloresCurso.values("curso_id").annotate(cuantos=Count('curso__area__color')).order_by('curso_id')
		cursosSemestres = cursosmalla.values("semestre").annotate(THCs=Sum('curso__horasClase'), THEEs=Sum('curso__horas'), TH=(Sum('curso__horas')+Sum('curso__horasClase')), TCR=Sum('curso__creditos'))		
		cursosprerequisitos = cursos.values("curso__nombre", "curso__id", "prerequisitos", "prerequisitos__nombre")
		cursoscorequisitos = cursos.values("curso__id", "corequisitos")
		print("aqui1")
		print(cursosprerequisitos)
		print(cursoscorequisitos)

		cursosra = cursos.values_list("curso__id").distinct().order_by('curso__id')
		cursosfg = cursos.values_list("curso__id", "formaciongeneral").distinct()

		for m in cursosmalla:
			m['curso'] = str(Curso.objects.get(id=m["curso"]).nombre)
		cursos2 =  json.dumps(list(cursosmalla), cls=DjangoJSONEncoder)
		cursosPorSemestre =  json.dumps(list(cursosSemestres), cls=DjangoJSONEncoder)
		cursosPre = json.dumps(list(cursosprerequisitos), cls=DjangoJSONEncoder)
		cursosCor = json.dumps(list(cursoscorequisitos), cls=DjangoJSONEncoder)
		cursosRA = json.dumps(list(cursosra), cls=DjangoJSONEncoder)
		cursosFG = json.dumps(list(cursosfg), cls=DjangoJSONEncoder)
		areaEscuela = json.dumps(list(areas2), cls=DjangoJSONEncoder)
		colorCurso = json.dumps(list(coloresCurso), cls=DjangoJSONEncoder)
		coloresCursoCuantos = json.dumps(list(coloresCursoCuantosSon), cls=DjangoJSONEncoder)

		print("aqui2")
		print(cursosRA)

		cursosSemestre = cursos.values("semestre")

		context = {'malla':malla,
		'programa':programa,
		'ayuda':ayuda,
		'resolucion': resolucion,
		'semestres':semestres,
		'cursos':cursos,
		'cursos2': cursos2,
		'cursosmalla':cursosmalla,
		'cursosSemestre':cursosSemestre,
		'cursosPre':cursosPre,
		'cursosCor':cursosCor,
		'areaEscuela':areaEscuela,
		'form2':form2,
		'areas2':areas2,
		#'cursosRA':cursosRA,
		'cursosFG':cursosFG,
		'cursosPorSemestre': cursosPorSemestre,
		'colorCurso': colorCurso,
		'coloresCursoCuantos': coloresCursoCuantos,
		}

		return render(request, 'mallacurricular/mallacurricularPDF.html', context)

class PdfFormacionCreateView(LoginRequiredMixin,TemplateView):
	def get(self,request,*args,**kwargs):
		form2 = ReporteCreateForm()
		malla = MallaCurricular.objects.get(id=kwargs['pk'])
		programa = malla.programa
		escuela = programa.unidadacademica
		semestres = Semestre.objects.filter(programa=programa).order_by('numero')
		cursos = CursosMalla.objects.filter(malla=malla)
		areas = Area.objects.filter(escuela=escuela)
		areasSN=areas.exclude(nombre='Ninguna')
		areas2 = areasSN.values("nombre", "color")
		cursosmalla = cursos.values("curso", "semestre", "id", "curso__creditos", "curso__horas", "curso__horasClase", "curso__tipo__abreviatura", "curso__codigo", "curso__area__color", "curso__ciclo__color", "curso__id", "curso__area__nombre", "curso__ciclo__nombre",)
		coloresCurso = cursos.values("curso__id", "curso__area__color")
		coloresCursoCuantosSon = coloresCurso.values("curso_id").annotate(cuantos=Count('curso__area__color')).order_by('curso_id')
		cursosSemestres = cursosmalla.values("semestre").annotate(THCs=Sum('curso__Clase'), THEEs=Sum('curso__'), TH=(Sum('curso__')+Sum('curso__Clase')), TCR=Sum('curso__creditos'))		
		cursosprerequisitos = cursos.values("curso__id", "prerequisitos")


		cursosra = cursos.values_list("curso__id").distinct().order_by('curso__id')
		cursosfg = cursos.values('formaciongeneral', 'curso_id', 'formaciongeneral__nombre', 'curso_id__nombre').distinct().order_by('formaciongeneral')

		for m in cursosmalla:
			m['curso'] = str(Curso.objects.get(id=m["curso"]).nombre)
		cursos2 =  json.dumps(list(cursosmalla), cls=DjangoJSONEncoder)
		cursosPorSemestre =  json.dumps(list(cursosSemestres), cls=DjangoJSONEncoder)
		cursosPre = json.dumps(list(cursosprerequisitos), cls=DjangoJSONEncoder)
		cursosRA = json.dumps(list(cursosra), cls=DjangoJSONEncoder)
		cursosFG = json.dumps(list(cursosfg), cls=DjangoJSONEncoder)
		areaEscuela = json.dumps(list(areas2), cls=DjangoJSONEncoder)
		colorCurso = json.dumps(list(coloresCurso), cls=DjangoJSONEncoder)
		coloresCursoCuantos = json.dumps(list(coloresCursoCuantosSon), cls=DjangoJSONEncoder)

		print("areas2")
		print(areas2)
		#THCsS1 = 0
		#for m in cursosSemestre1:
		#	THCsS1 = THCsS1 + int(cursosSemestre1.id[m])

		#print(THCsS1)
		#print(cursosSemestreUno)
		#cursosSemestre = 
		#cursos2 = json.dumps(list(cursos), cls=DjangoJSONEncoder)
		cursosSemestre = cursos.values("semestre")
		#print(cursosSemestre)
		#cursos2 = json.dumps(list(cursos), cls=DjangoJSONEncoder)
		context = {'malla':malla,
		'programa':programa,
		'semestres':semestres,
		'cursos':cursos,
		'cursos2': cursos2,
		'cursosmalla':cursosmalla,
		'cursosSemestre':cursosSemestre,
		'cursosPre':cursosPre,
		'areaEscuela':areaEscuela,
		'form2':form2,
		#'cursosRA':cursosRA,
		'cursosFG':cursosFG,
		'cursosPorSemestre': cursosPorSemestre,
		'colorCurso': colorCurso,
		'coloresCursoCuantos': coloresCursoCuantos,
		}

		return render(request, 'mallacurricular/formacionPDF.html',context)

class PdfFormacionGeneralCreateView(LoginRequiredMixin,TemplateView):
	def get(self,request,*args,**kwargs):
		form2 = ReporteCreateForm()
		malla = MallaCurricular.objects.get(id=kwargs['pk'])
		programa = malla.programa
		resolucion = malla.resolucion
		ayuda = malla.ayuda
		escuela = programa.unidadacademica
		semestres = Semestre.objects.filter(programa=programa).order_by('numero')
		cursos = CursosMalla.objects.filter(malla=malla)
		areas = Area.objects.filter(escuela=escuela)
		areasSN=areas.exclude(nombre='Ninguna')
		areas2 = areasSN.values("nombre", "color")
		cursosmalla = cursos.values("curso", "semestre", "id", "curso__creditos", "curso__horas", "curso__horasClase", "curso__tipo__abreviatura", "curso__codigo", "curso__area__color", "curso__ciclo__color", "curso__id", "curso__area__nombre", "curso__ciclo__nombre", "formaciongeneral__nombre")
		coloresCurso = cursos.values("curso__id", "curso__area__color")
		coloresCursoCuantosSon = coloresCurso.values("curso_id").annotate(cuantos=Count('curso__area__color')).order_by('curso_id')
		cursosSemestres = cursosmalla.values("semestre").annotate(THCs=Sum('curso__horasClase'), THEEs=Sum('curso__horas'), TH=(Sum('curso__horas')+Sum('curso__horasClase')), TCR=Sum('curso__creditos'))		
		cursosprerequisitos = cursos.values("curso__id", "prerequisitos")
		print("aqui1")
		print(malla)

		cursosra = cursos.values_list("curso__id").distinct().order_by('curso__id')
		cursosfg = cursos.values_list("curso__id", "formaciongeneral").distinct()

		for m in cursosmalla:
			m['curso'] = str(Curso.objects.get(id=m["curso"]).nombre)
		cursos2 =  json.dumps(list(cursosmalla), cls=DjangoJSONEncoder)
		cursosPorSemestre =  json.dumps(list(cursosSemestres), cls=DjangoJSONEncoder)
		cursosPre = json.dumps(list(cursosprerequisitos), cls=DjangoJSONEncoder)
		cursosRA = json.dumps(list(cursosra), cls=DjangoJSONEncoder)
		cursosFG = json.dumps(list(cursosfg), cls=DjangoJSONEncoder)
		areaEscuela = json.dumps(list(areas2), cls=DjangoJSONEncoder)
		colorCurso = json.dumps(list(coloresCurso), cls=DjangoJSONEncoder)
		coloresCursoCuantos = json.dumps(list(coloresCursoCuantosSon), cls=DjangoJSONEncoder)

		print("aqui2")
		print(cursosRA)

		cursosSemestre = cursos.values("semestre")

		context = {'malla':malla,
		'programa':programa,
		'ayuda':ayuda,
		'resolucion': resolucion,
		'semestres':semestres,
		'cursos':cursos,
		'cursos2': cursos2,
		'cursosmalla':cursosmalla,
		'cursosSemestre':cursosSemestre,
		'cursosPre':cursosPre,
		'areaEscuela':areaEscuela,
		'form2':form2,
		#'cursosRA':cursosRA,
		'cursosFG':cursosFG,
		'cursosPorSemestre': cursosPorSemestre,
		'colorCurso': colorCurso,
		'coloresCursoCuantos': coloresCursoCuantos,
		}
		return render(request, 'mallacurricular/formaciongeneralPDF.html',context)


class PdfTotalCreateView(LoginRequiredMixin,TemplateView):
	def get(self,request,*args,**kwargs):
		form2 = ReporteCreateForm()
		malla = MallaCurricular.objects.get(id=kwargs['pk'])
		programa = malla.programa
		escuela = programa.unidadacademica
		semestres = Semestre.objects.filter(programa=programa).order_by('numero')
		cursos = CursosMalla.objects.filter(malla=malla)
		areas = Area.objects.filter(escuela=escuela)
		areasSN=areas.exclude(nombre='Ninguna')
		areas2 = areasSN.values("nombre", "color")
		cursosmalla = cursos.values("curso", "semestre", "id", "curso__creditos", "curso__horas", "curso__horasClase", "curso__tipo__abreviatura", "curso__codigo", "curso__area__color", "curso__ciclo__color", "curso__id", "curso__area__nombre", "curso__ciclo__nombre", "curso__tipo__nombre")
		coloresCurso = cursos.values("curso__id", "curso__area__color")
		coloresCursoCuantosSon = coloresCurso.values("curso_id").annotate(cuantos=Count('curso__area__color')).order_by('curso_id')
		cursosSemestres = cursosmalla.values("semestre").annotate(THCs=Sum('curso__horasClase'), THEEs=Sum('curso__horas'), TH=(Sum('curso__horas')+Sum('curso__horasClase')), TCR=Sum('curso__creditos'))		
		cursosprerequisitos = cursos.values("curso__id", "prerequisitos")
		creditosTotales = cursosmalla.aggregate(Sum('curso__creditos'))
		ciclos = cursosmalla.values("curso__ciclo__nombre").annotate(cuantos=Sum("curso__creditos"), cuantos2=Sum("curso__ciclo")).order_by(Coalesce('cuantos', 'cuantos').desc())
		print(ciclos)
		tipoasignatura = cursosmalla.values("curso__tipo__nombre").annotate(cuantostipos=Sum("curso__creditos"), cuantostp2=Sum("curso__tipo")).order_by(Coalesce('cuantostipos', 'cuantostipos').desc())
		formaciongeneral = cursosmalla.values("formaciongeneral__nombre").annotate(cuantosfg=Sum("curso__creditos"), cuantosfg2=Sum("formaciongeneral")).order_by(Coalesce('cuantosfg', 'cuantosfg').desc())
		cursosra = cursos.values_list("curso__id").distinct().order_by('curso__id')
		cursosfg = cursos.values('formaciongeneral', 'curso_id', 'formaciongeneral__nombre', 'curso_id__nombre').distinct().order_by('formaciongeneral')


		for m in cursosmalla:
			m['curso'] = str(Curso.objects.get(id=m["curso"]).nombre)
		cursos2 =  json.dumps(list(cursosmalla), cls=DjangoJSONEncoder)
		cursosPorSemestre =  json.dumps(list(cursosSemestres), cls=DjangoJSONEncoder)
		cursosPre = json.dumps(list(cursosprerequisitos), cls=DjangoJSONEncoder)
		cursosRA = json.dumps(list(cursosra), cls=DjangoJSONEncoder)
		cursosFG = json.dumps(list(cursosfg), cls=DjangoJSONEncoder)
		areaEscuela = json.dumps(list(areas2), cls=DjangoJSONEncoder)
		colorCurso = json.dumps(list(coloresCurso), cls=DjangoJSONEncoder)
		coloresCursoCuantos = json.dumps(list(coloresCursoCuantosSon), cls=DjangoJSONEncoder)
		ciclosCreditos = json.dumps(list(ciclos), cls=DjangoJSONEncoder)
		tipoCreditos = json.dumps(list(tipoasignatura), cls=DjangoJSONEncoder)
		fgCreditos = json.dumps(list(formaciongeneral), cls=DjangoJSONEncoder)
		#totalCreditos = json.dumps(list(creditosTotales), cls=DjangoJSONEncoder)
		print("cursostipos")
		print(tipoCreditos)

		cursosSemestre = cursos.values("semestre")

		context = {'malla':malla,
		'programa':programa,
		'semestres':semestres,
		'cursos':cursos,
		'cursos2': cursos2,
		'cursosmalla':cursosmalla,
		'cursosSemestre':cursosSemestre,
		'cursosPre':cursosPre,
		'areaEscuela':areaEscuela,
		'form2':form2,
		#'cursosRA':cursosRA,
		'cursosFG':cursosFG,
		'cursosPorSemestre': cursosPorSemestre,
		'colorCurso': colorCurso,
		'coloresCursoCuantos': coloresCursoCuantos,
		'ciclosCreditos':ciclosCreditos,
		'ciclos':ciclos,
		'tipoCreditos': tipoCreditos,
		'tipoasignatura':tipoasignatura,
		'formaciongeneral': formaciongeneral,
		'fgCreditos':fgCreditos,
		'creditosTotales':creditosTotales,
		}
		return render(request, 'mallacurricular/totalPDF.html',context)