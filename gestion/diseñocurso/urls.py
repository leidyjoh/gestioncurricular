 # -*- coding: utf-8 -*-
from django.urls import path
from .views import DiseñoCursoListView, DiseñoCursoPDFCreateView
from .forms import DiseñoCursoCreateView, DiseñoMicroCreateView

app_name = 'diseñocurso'

urlpatterns = [

	path('diseñocurso/crear/', DiseñoCursoCreateView.as_view(), name='crear'),
	path('diseñocurso/listar/', DiseñoCursoListView.as_view(), name='listar'),
	path('diseñocurso/PDF/(<int:pk>)', DiseñoCursoPDFCreateView.as_view(), name='cursodiseñopdf'),
	path('diseñocurso/crearmicro/(<int:pk>)', DiseñoMicroCreateView.as_view(), name='crearmicro'),



]