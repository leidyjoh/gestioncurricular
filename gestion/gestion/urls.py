"""gestion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import include, path
from django.contrib import admin
from gestion import settings
from django.views.static import serve
from inicio import views
from django.conf.urls import handler404, handler500



urlpatterns = [
    path('admin/', admin.site.urls),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    #url para acceder a la imagenes que estan en la carpeta media del proyecto
    #path('media/(?P<path>.*)', serve, {'document_root': settings.MEDIA_ROOT}),
] 

#+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
handler404 = 'inicio.views.pag_404_not_found'
handler500 = 'inicio.views.pag_500_error_server'