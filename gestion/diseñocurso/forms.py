# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView
from django.template import loader
from django.template import Context
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse
from django.contrib import messages
from django import forms
from inicio.mixins import LoginRequiredMixin
from mallacurricular.models import CursosMalla
from .models import DiseñoCurso, Microcurriculo
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget
import json
from django.http import HttpResponseRedirect

"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""
class DiseñoCursoForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""
	
	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = DiseñoCurso
		fields = ['docente', 'curso']



"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class DiseñoCursoCreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = DiseñoCursoForm
	model = DiseñoCurso


	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para crear un VehículoEspecial"""
		context = super(DiseñoCursoCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nueva Asignación del Diseños de Curso'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"La asignación del diseño del curso ha sido creado con exito")
		return reverse_lazy('diseñocurso:listar')


def crearDiseñoMicro(curso):
	class DiseñoMicroForm(LoginRequiredMixin, forms.ModelForm):
		"""Permite crear un formulario con los campos del facultad"""
		#curso = forms.CharField(disabled=True)
		
		class Meta:
			"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
			model = Microcurriculo
			fields = ['curso', 'ejes', 'validable', 'habilitable']

		def __init__(self, *args, **kwargs):
			#pk = kwargs.pop('pk')
			super().__init__(*args, **kwargs)
			#self.fields['curso'].queryset = CursosMalla.objects.all()
	return DiseñoMicroForm


class DiseñoMicroCreateView(LoginRequiredMixin, CreateView):

	def get(self, request, *args, **kwargs):
		pcurso = DiseñoCurso.objects.get(id=kwargs['pk']) 
		curso = pcurso.curso
		curso_id = curso.id
		print("curso_id")
		print(curso_id)

		DiseñoMicroForm = crearDiseñoMicro(curso)
		formRA = DiseñoMicroForm()

		forms = [formRA]
		context = {'curso':curso,
		'formRA':formRA}

		return render(request, 'diseñocurso/microcurriculo_form.html',context)

	def post(self, request, *args, **kwargs):
		pcurso = DiseñoCurso.objects.get(id=kwargs['pk']) 
		curso = pcurso.curso
		print("AYUDA1")
		print(curso)

		DiseñoMicroForm = crearDiseñoMicro(curso)
		formRA = DiseñoMicroForm(request.POST)

		if formRA.is_valid():
			formRA.instance.curso = curso
			print("AYUDA2")
			print(curso)
			self.objects = formRA.save()
			#formRA.save()
			url = reverse('diseñocurso:listar')
			messages.info(self.request,'Nuevo diseño del curso creado con exito')
			return HttpResponseRedirect(url)

		formRA = DiseñoMicroForm(self.request.POST)
		messages.info(self.request,'Hay errores en algun campo, revise el formulario')

		context = {
		'section_title':'Nuevo Diseño de Curso',
		'formRA':formRA, }
		
		return render(self.request, 'diseñocurso/microcurriculo_form.html', context)

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class DiseñoCursoUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = DiseñoCurso
	fields = ['docente', 'curso']
	success_url = reverse_lazy('diseñocurso:listar')
	

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(DiseñoCursoUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Asigna'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"La facultad ha sido actualizada con exito")
		return reverse_lazy('facultad:listar')