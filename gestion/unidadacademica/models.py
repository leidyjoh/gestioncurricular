# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from facultad.models import Facultad

#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla UnidadAcademica en base de datos"""
class UnidadAcademica(models.Model):
	"""Permite definir un listado de UnidadesAcademicas"""

	#Nombre de la unidad academica
	nombre = models.CharField(max_length=200)
	#Facultad a la que pertenece
	facultad = models.ForeignKey(Facultad, on_delete=models.CASCADE)


	"""Permite ordenar la lista de unidades academicas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Unidades Academicas"

	#Permite determinar una representación en string del objeto Unidad Academica
	def __str__(self):
		return self.nombre

	"""Permite determinar una representación en string para el objeto Unidad Academica (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.nombre