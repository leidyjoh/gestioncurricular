# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import TemplateView
from django.template import loader
from django.template import Context
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse
from django.contrib import messages
from django import forms
from inicio.mixins import LoginRequiredMixin
from .models import ComponenteFacultad
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget
import json

"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""
class ComponenteFacultadCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""
	descripcion = forms.CharField(widget=SummernoteWidget(), label='Descripción')

	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = ComponenteFacultad
		fields = ['numero', 'descripcion', 'scc']

"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class ComponenteFacultadCreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = ComponenteFacultadCreateForm
	model = ComponenteFacultad

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para crear un VehículoEspecial"""
		context = super(ComponenteFacultadCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nuevo Componente de la Facultad'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El Componente de la facultad ha sido creada con exito")
		return reverse_lazy('componentefacultad:listar')

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class ComponenteFacultadUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = ComponenteFacultad
	fields = ['numero', 'descripcion', 'scc']
	success_url = reverse_lazy('componentefacultad:listar')

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(ComponenteFacultadUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Componente de la Facultad'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El componente de la facultad ha sido actualizada con exito")
		return reverse_lazy('componentefacultad:listar')