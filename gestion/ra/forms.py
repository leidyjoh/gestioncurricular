# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import TemplateView
from django.template import loader
from django.template import Context
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse
from django.contrib import messages
from django import forms
from inicio.mixins import LoginRequiredMixin
from programa.models import Programa
from perfil.models import Competencias
from matriztributacion.models import CursosTributacionMatriz
from mallacurricular.models import CursosMalla
from diseñocurso.models import DiseñoCurso
from .models import ResultadoAprendizaje
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget
import json
from django.http import HttpResponseRedirect

"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""

def crearRA(curso):

	class ResultadoAprendizajeCreateForm(LoginRequiredMixin, forms.ModelForm):

		descripcion = forms.CharField(widget=SummernoteWidget(), label='Descripción')
		scc = forms.ModelChoiceField(queryset = CursosTributacionMatriz.objects.all(), label='SCC')

		def __init__(self, *args, **kwargs):
			super(ResultadoAprendizajeCreateForm, self).__init__(*args, **kwargs)           

			#self.fields['programa'].queryset = CursosMalla.objects.get(curso_id=curso)
			self.fields['scc'].queryset = CursosTributacionMatriz.objects.filter(curso__curso__nombre=curso)

		class Meta:
			"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
			model = ResultadoAprendizaje
			widgets = {'descripcion': SummernoteWidget()}
			fields = ['id', 'numero','accion', 'contenido', 'contexto', 'finalidad', 'descripcion', 'nivelAprendizaje', 'programa', 'curso', 'porcentaje', 'scc', 'posiblesActividades']

	return ResultadoAprendizajeCreateForm



	
"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class RACreateView(LoginRequiredMixin, CreateView):

	def get(self, request, *args, **kwargs):
		pcurso = DiseñoCurso.objects.get(id=kwargs['pk']) 
		curso = pcurso.curso
		curso_id = curso.id
		print("curso_id")
		print(curso_id)

		competencias = CursosTributacionMatriz.objects.filter(curso__curso__id=curso_id)
		ResultadoAprendizajeCreateForm = crearRA(curso)
		formRA = ResultadoAprendizajeCreateForm(prefix='competencias')

		forms = [formRA]
		context = {'curso':curso,
		'competencias':competencias,
		'formRA':formRA}

		return render(request, 'ra/resultadoaprendizaje_form.html',context)

	def post(self, request, *args, **kwargs):
		pcurso = DiseñoCurso.objects.get(id=kwargs['pk']) 
		curso = pcurso.curso
		print("AYUDA1")
		print(curso)
		programa = self.request.user.profesor.programa

		competencias = CursosTributacionMatriz.objects.filter(curso=curso)
		ResultadoAprendizajeCreateForm = crearRA(curso)
		formRA = ResultadoAprendizajeCreateForm(request.POST, prefix='competencias')

		if formRA.is_valid():
			formRA.instance.curso = curso
			print("AYUDA2")
			print(curso)
			formRA.instance.programa = self.request.user.profesor.programa
			self.objects = formRA.save()
			#formRA.save()
			url = reverse('resultadoaprendizaje:listar')
			messages.info(self.request,'Nuevo ra creado con exito')
			return HttpResponseRedirect(url)

		formRA = ResultadoAprendizajeCreateForm(self.request.POST, prefix='competencias')
		messages.info(self.request,'Hay errores en algun campo, revise el formulario')

		context = {
		'section_title':'Nuevo Perfil',
		'formRA':formRA, }
		
		return render(self.request, 'ra/resultadoaprendizaje_form.html', context)

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class RAUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = ResultadoAprendizaje
	fields = ['id', 'numero','accion', 'contenido', 'contexto', 'finalidad', 'descripcion', 'nivelAprendizaje', 'programa', 'curso']
	success_url = reverse_lazy('ra:listar')

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(RAUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Resultado de Aprendizaje'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El resultado de aprendizaje ha sido actualizado con exito")
		return reverse_lazy('ra:listar')
