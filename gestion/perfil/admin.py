# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
from .models import Perfil, AmbitosPrograma, ProgramaEducativo, Competencias


"""Permite administrar la visualización de los datos de Perfil en la base de datos del
	sitio de administración"""
class AdminPerfil(SummernoteModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Perfil que se mostrarán en el sitio de administración"""
	list_display = ('id', 'programa', 'resolucion', 'descripcion', 'get_sccFacultad', 'leyendaambitos', 'leyendasccgenericas', 'leyendasccespecificas')
	summernote_fields = ('descripcion',)
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Perfil en el sitio 
	de administración"""
	search_fields = ('id', 'programa')

#Permite registrar las clases Perfil y AdminPerfil
admin.site.register(Perfil, AdminPerfil)


"""Permite administrar la visualización de los datos de Perfil en la base de datos del
	sitio de administración"""
class AdminAmbitosPrograma(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Perfil que se mostrarán en el sitio de administración"""
	list_display = ('id', 'titulo', 'perfil',)
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Perfil en el sitio 
	de administración"""
	search_fields = ('id', 'titulo')

#Permite registrar las clases Perfil y AdminPerfil
admin.site.register(AmbitosPrograma, AdminAmbitosPrograma)


"""Permite administrar la visualización de los datos de Perfil en la base de datos del
	sitio de administración"""
class AdminProgramaEducativo(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Perfil que se mostrarán en el sitio de administración"""
	list_display = ('id', 'programa', 'documento',)
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Perfil en el sitio 
	de administración"""
	search_fields = ('id', 'programa')

#Permite registrar las clases Perfil y AdminPerfil
admin.site.register(ProgramaEducativo, AdminProgramaEducativo)


"""Permite administrar la visualización de los datos de Perfil en la base de datos del
	sitio de administración"""
class AdminCompetencias(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Perfil que se mostrarán en el sitio de administración"""
	list_display = ('id', 'titulo', 'perfil',)
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Perfil en el sitio 
	de administración"""
	search_fields = ('id', 'titulo')

#Permite registrar las clases Perfil y AdminPerfil
admin.site.register(Competencias, AdminCompetencias)