# -*- coding: utf-8 -*-

from django.db import models

# Create your models here.
from django.db import models
from unidadacademica.models import UnidadAcademica
from sccfacultad.models import SCCFacultad


#Tipo de programa
PREGRADO = 'Pregrado'
POSGRADO = 'Posgrado'

tipo_programa = (
	(PREGRADO, 'Pregrado'),
	(POSGRADO, 'Pogrado'),
)

#Tipo de jornada
DIURNA = 'Diurna'
NOCTURNA = 'Nocturna'

tipo_jornada = (
	(DIURNA, 'Diurna'),
	(NOCTURNA, 'Nocturna'),
)
#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class Programa(models.Model):
	"""Permite definir un listado de Programas"""

	#Nombre del programa
	nombre = models.CharField(max_length=200)
	#Código del programa
	codigo = models.PositiveIntegerField(verbose_name= ('Código'))
	#Unidad académica al que pertenece
	unidadacademica =models.ForeignKey(UnidadAcademica,  on_delete=models.CASCADE, verbose_name= ('Unidad Académica'))
	#Nivel de programa
	nivel = models.CharField(max_length=50, choices=tipo_programa, default=PREGRADO, verbose_name= ('Nivel Académico'))
	#Jornada
	jornada = models.CharField(max_length=50, choices=tipo_jornada, default=DIURNA)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Programas"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.nombre + " - " + self.jornada

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.nombre + " - " + self.jornada


#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class ProgramaSemestre(models.Model):
	"""Permite definir un listado de Programas"""

	#Nombre del programa
	programa = models.CharField(max_length=200)
	#numero del semestre
	semestre = models.PositiveIntegerField()


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['programa']
		verbose_name_plural = "Semestres por Programas"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.programa

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.programa



#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla UnidadAcademica en base de datos"""
class SCCFacultadPrograma(models.Model):
	"""Permite definir un listado de UnidadesAcademicas"""

	sccsfacultad = models.ForeignKey(SCCFacultad, on_delete=models.CASCADE)
	#Programa al que pertenece
	programa = models.ForeignKey(Programa, on_delete=models.CASCADE, related_name="programa_academico")
	

	"""Permite ordenar la lista de perfiles por programa y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['programa']
		verbose_name_plural = "SCC Facultad por Programa"

	#Permite determinar una representación en string del objeto Perfil
	def __str__(self):
		return str(self.sccsfacultad)

	"""Permite determinar una representación en string para el objeto Perfil(Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.sccsfacultad)
