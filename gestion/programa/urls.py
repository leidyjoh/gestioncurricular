 # -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import ProgramaCreateView, ProgramaUpdateView, ProgramaDeleteView
from .views import ProgramaListView

app_name = 'programa'

urlpatterns = [

	path('programa/crear/', ProgramaCreateView.as_view(), name='crear'),
	path('programa/listar/', ProgramaListView.as_view(), name='listar'),
	path('programa/actualizar/<int:pk>', ProgramaUpdateView.as_view(), name='actualizar'),
	path('programa/eliminar/<int:pk>', ProgramaDeleteView.as_view(), name='eliminar'),

]