# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import ListView, TemplateView
from django.views.generic.detail import DetailView
from .models import Perfil, AmbitosPrograma, ProgramaEducativo
from inicio.mixins import LoginRequiredMixin
from django.shortcuts import render
from sccfacultad.models import SCCFacultad
from scc.models import SCC, SCCEspecificas
from programa.models import SCCFacultadPrograma


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class PerfilListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = Perfil
	context_object_name = 'perfiles'
	template_name = 'perfil/perfil_list.html'

	def get_queryset(self):
		#persona = self.kwargs['pk']
		#unidad = user.unidadacademica
		if self.request.user.profesor.cargoAdministrativo == "Director de Escuela":
			return Perfil.objects.filter(programa__unidadacademica=self.request.user.profesor.unidadAcademica)
		elif self.request.user.profesor.cargoAdministrativo == "Director de Programa":
			return Perfil.objects.filter(programa=self.request.user.profesor.programa)
		elif self.request.user.profesor.cargoAdministrativo == "Docente":
			return Perfil.objects.filter(programa__unidadacademica=self.request.user.profesor.unidadAcademica)

"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class AmbitosListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = AmbitosPrograma
	context_object_name = 'ambitosperfil'
	template_name = 'perfil/perfil_list.html'

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		context = super(AmbitosListView,self).get_context_data(**kwargs)
		return context

"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class ProgramaEducativoListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = ProgramaEducativo
	context_object_name = 'programaseducativos'
	template_name = 'perfil/programaeducativo_list.html'

	def get_queryset(self):
		#persona = self.kwargs['pk']
		#unidad = user.unidadacademica
		if self.request.user.profesor.cargoAdministrativo == "Director de Escuela":
			return ProgramaEducativo.objects.filter(programa__unidadacademica=self.request.user.profesor.unidadAcademica)
		elif self.request.user.profesor.cargoAdministrativo == "Director de Programa":
			return ProgramaEducativo.objects.filter(programa__unidadacademica=self.request.user.profesor.unidadAcademica)
"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class SCCFacultadListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = SCCFacultad
	context_object_name = 'sccsfacultad'
	template_name = 'perfil/perfil_list.html'

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		context = super(SCCFacultadListView,self).get_context_data(**kwargs)
		return context


class PdfPerfilCreateView(LoginRequiredMixin,TemplateView):
	def get(self,request,*args,**kwargs):
		perfil= Perfil.objects.get(id=kwargs['pk'])
		programa=perfil.programa
		ambitos= AmbitosPrograma.objects.filter(perfil=perfil)
		sccs=perfil.sccFacultad.all()
		sccsgenericas=SCC.objects.filter(perfil=perfil)
		sccsespecificas=SCCEspecificas.objects.filter(perfil=perfil)
		context = {'perfil':perfil,
		'ambitos':ambitos,
		'sccsgenericas':sccsgenericas,
		'sccsespecificas':sccsespecificas,
		'sccs':sccs}

		return render(request, 'perfil/perfilPDF.html',context)