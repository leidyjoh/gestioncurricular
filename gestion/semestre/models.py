# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from programa.models import Programa


#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class Semestre(models.Model):
	"""Permite definir un listado de Semestres"""

	#Numero del semestre
	numero = models.PositiveIntegerField(verbose_name= ('Número'))
	#Programa del semestre
	programa = models.ForeignKey(Programa, on_delete=models.CASCADE)


	"""Permite ordenar la lista de semestres por numero y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['numero']
		verbose_name_plural = "Semestres"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return str(self.numero)

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.numero)