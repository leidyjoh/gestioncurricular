# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from colorfield.fields import ColorField
#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Facultad en base de datos"""
class Facultad(models.Model):
	"""Permite definir un listado de Facultades"""

	#Nombre de la facultad
	nombre = models.CharField(max_length=200)

	"""Permite ordenar la lista de facultades por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Facultades"

	#Permite determinar una representación en string del objeto Facultad
	def __str__(self):
		return self.nombre

	"""Permite determinar una representación en string para el objeto Facultad (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.nombre


#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class CicloAsignatura(models.Model):
	"""Permite definir un listado de Cursos"""

	#Nombre de la asignatura
	nombre = models.CharField(max_length=200)
	#escuela que lo gestiona
	color = ColorField(default='#FF0000')
	#escuela que lo gestiona
	facultad = models.ForeignKey(Facultad, on_delete=models.CASCADE)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Ciclo Asignatura"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.nombre

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.nombre

#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class TipoAsignatura(models.Model):
	"""Permite definir un listado de Cursos"""

	#Nombre de la asignatura
	nombre = models.CharField(max_length=200)
	#Nombre de la asignatura
	abreviatura = models.CharField(max_length=4)
	#escuela que lo gestiona
	facultad = models.ForeignKey(Facultad, on_delete=models.CASCADE)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Tipo Asignatura"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.abreviatura

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.abreviatura

class FormacionGeneral(models.Model):
	"""Permite definir un listado de Cursos"""

	#Nombre de la formación
	nombre = models.CharField(max_length=200)
	#Abreviatura de la formación
	abreviatura = models.CharField(max_length=4)
	#escuela que lo gestiona
	facultad = models.ForeignKey(Facultad, on_delete=models.CASCADE)
	#escuela que lo gestiona
	color = ColorField(default='#FF0000')


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Formación General"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.nombre

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.nombre


class NivelAprendizaje(models.Model):
	"""Permite definir un listado de Cursos"""

	#Nombre de la formación
	nombre = models.CharField(max_length=200)
	#escuela que lo gestiona
	facultad = models.ForeignKey(Facultad, on_delete=models.CASCADE)



	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Nivel de Aprendizaje"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.nombre

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.nombre