# -*- coding: utf-8 -*-

from django import forms
from tenants.models import Tenant

class TenantCreateForm(forms.ModelForm):

    class Meta:
        model = Tenant
        fields = ['nombre']

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.schema_name = subdomain.replace('-', '_')
        # You need to replace localhost for your domain
        instance.domain_url = f'{subdomain}.localhost'
        instance.save()
        return instance