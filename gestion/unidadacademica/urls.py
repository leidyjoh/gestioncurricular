 # -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import UnidadAcademicaCreateView, UnidadAcademicaUpdateView, UnidadAcademicaDeleteView
from .views import UnidadAcademicaListView

app_name = 'unidadacademica'

urlpatterns = [

	path('unidadacademica/crear/', UnidadAcademicaCreateView.as_view(), name='crear'),
	path('unidadacademica/listar/', UnidadAcademicaListView.as_view(), name='listar'),
	path('unidadacademica/actualizar/<int:pk>', UnidadAcademicaUpdateView.as_view(), name='actualizar'),
	path('unidadacademica/eliminar/<int:pk>', UnidadAcademicaDeleteView.as_view(), name='eliminar'),
]