# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import IndicadorLogro

"""Permite administrar la visualización de los datos de Programa en la base de datos del
	sitio de administración"""
class AdminIndicadorLogro(admin.ModelAdmin):
	"""Permite establecer la información de los datos id, nombre, codigo, unidadacademica y tipo del 
	modelo Programa que se mostrarán en el sitio de administración"""
	list_display = ('id', 'numero','accion', 'contenido', 'contexto', 'descripcion', 'resultadoAprendizaje')
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Programa 
	en el sitio de administración"""
	search_fields = ('numero', 'descripcion')

#Permite registrar las clases Programa y AdminPrograma
admin.site.register(IndicadorLogro, AdminIndicadorLogro)

# Register your models here.
