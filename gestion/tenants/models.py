# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from tenant_schemas.models import TenantMixin

#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Facultad en base de datos"""
class Tenant(TenantMixin):
	"""Permite definir un listado de Facultades"""

	#Nombre del tenants
	nombre = models.CharField(max_length=200)
	# default true, schema will be automatically created and synced when it is saved
	auto_create_schema = True


	"""Permite ordenar la lista de facultades por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Tenants"

	#Permite determinar una representación en string del objeto Facultad
	#def __str__(self):
		#return self.nombre

	"""Permite determinar una representación en string para el objeto Facultad (Para 
	versiones de Python 2)"""
	#def __unicode__(self):
	#	return self.nombre