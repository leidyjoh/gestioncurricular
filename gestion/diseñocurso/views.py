# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from django.db.models import Count, Sum
from django.db.models.functions import Coalesce
from django.views.generic import ListView, TemplateView
from django.views.generic.detail import DetailView
from programa.models import Programa
from usuario.models import Profesor
from facultad.models import TipoAsignatura
from .models import DiseñoCurso, Microcurriculo
from django import forms
from matriztributacion.models import CursosTributacionMatriz
from curso.models import Curso, Area
from ra.models import ResultadoAprendizaje
from mallacurricular.models import CursosMalla
from semestre.models import Semestre
from inicio.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.db.models import Count, Sum
from django.db.models.functions import Coalesce
from django.core.serializers.json import DjangoJSONEncoder
import json
from django.db.models.query import QuerySet
from django_group_by import GroupByMixin


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class DiseñoCursoListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = DiseñoCurso
	context_object_name = 'cursosasignados'
	template_name = 'diseñocurso/cursodiseño_list.html'

	def get_queryset(self):
		if self.request.user.profesor.cargoAdministrativo == "Director de Programa":
			return DiseñoCurso.objects.filter(docente__programa=self.request.user.profesor.programa)
		elif self.request.user.profesor.cargoAdministrativo == "Ninguno":
			return DiseñoCurso.objects.filter(docente=self.request.user.profesor)


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
"""class DiseñoCursoListView(LoginRequiredMixin, ListView):

	model = CursosMalla
	context_object_name = 'cursosasignados'
	template_name = 'diseñocurso/diseñocurso_list.html'

	def get_queryset(self):
		if self.request.user.profesor.cargoAdministrativo == "Director de Programa":
			return CursosMalla.objects.filter(curso__malla__programa=self.request.user.profesor.programa)
		elif self.request.user.profesor.cargoAdministrativo == "Ninguno":
			return DiseñoCurso.objects.filter(docente=self.request.user.profesor)
"""

class DiseñoCursoPDFCreateView(LoginRequiredMixin,TemplateView):
	def get(self,request,*args,**kwargs):
		pcurso = DiseñoCurso.objects.get(id=kwargs['pk']) 
		curso = pcurso.curso
		curso_id = curso.id
		creditos = curso.curso.creditos
		print("AYUDA")
		print(creditos)
		horasT = curso.curso.horas
		horas = curso.curso.horasClase
		escuela = curso.curso.escuela
		descripcion = curso.curso.descripcion
		malla = curso.malla
		programa = malla.programa
		tipo = curso.curso.tipo
		dcurso = Microcurriculo.objects.get(curso=curso)
		cursod = dcurso.curso
		habilitable = dcurso.habilitable
		validable = dcurso.validable
		formacion = dcurso.validable
		descripcion = curso.curso.descripcion
		ejes = dcurso.ejes
		sccsgenericas=CursosTributacionMatriz.objects.filter(curso=curso)
		scc = CursosTributacionMatriz.objects.filter(curso=curso)
		sccs = scc.values("scc__titulo", "curso__resultadoaprendizaje__descripcion")
		#sccs = CursosTributacionMatriz.raw('SELECT * FROM CursosTributacionMatriz GROUP BY scc__titulo')
		sccs2 = json.dumps(list(sccs), cls=DjangoJSONEncoder)
		print("AYUDA")
		print(sccs)
		ra = ResultadoAprendizaje.objects.filter(curso=curso)
		ras1 = ra.values("descripcion")
		ras2 = json.dumps(list(ras1), cls=DjangoJSONEncoder)
		ras3 = ra.values("porcentaje")
		ras4 = json.dumps(list(ras3), cls=DjangoJSONEncoder)
		"""
		print(nombre)
		
		ra = ResultadoAprendizaje.objects.filter(curso=curso)
		ras = ra.values("curso", "descripcion", "porcentaje")
		ras2 = json.dumps(list(ras), cls=DjangoJSONEncoder)

		#ra =
		
		#mallacurso = CursosMalla.objects.filter(malla__programa=self.request.user.profesor.programa, curso__nombre=curso)

		#cursomalla = curso.value_list("curso", "id", "curso__nombre", "curso__")
		#curso2 =  json.dumps(list(cursomalla), cls=DjangoJSONEncoder)


"""

		context = {'curso':curso,
		'creditos':creditos,
		'horasT':horasT,
		'horas':horas,
		'escuela':escuela,
		'programa':programa,
		'tipo':tipo,
		'validable':validable,
		'habilitable':habilitable,
		'formacion':formacion,
		'descripcion': descripcion,
		'ejes': ejes,
		'sccsgenericas':sccsgenericas,
		'sccs':sccs,
		'sccs2':sccs2,
		'ras1':ras1,
		'ras2':ras2,
		'ras3':ras3,
		'ras4':ras4,

		}

		return render(request, 'diseñocurso/cursodiseñoPDF.html',context)