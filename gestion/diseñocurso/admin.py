# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import DiseñoCurso, Microcurriculo


"""Permite administrar la visualización de los datos de Facultad en la base de datos del
	sitio de administración"""
class AdminDiseñoCurso(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Facultad que se mostrarán en el sitio de administración"""
	list_display = ('id', 'docente', 'curso')
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Facultad en el sitio 
	de administración"""
	search_fields = ('curso',)

#Permite registrar las clases Facultad y AdminFacultad
admin.site.register(DiseñoCurso, AdminDiseñoCurso)


"""Permite administrar la visualización de los datos de Facultad en la base de datos del
	sitio de administración"""
class AdminMicro(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Facultad que se mostrarán en el sitio de administración"""
	list_display = ('id', 'curso', 'ejes', 'validable', 'habilitable')
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Facultad en el sitio 
	de administración"""
	search_fields = ('curso',)

#Permite registrar las clases Facultad y AdminFacultad
admin.site.register(Microcurriculo, AdminMicro)