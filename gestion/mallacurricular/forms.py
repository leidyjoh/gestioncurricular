# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import TemplateView
from django.template import loader
from django.template import Context
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django import forms
from inicio.mixins import LoginRequiredMixin
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget
from .models import MallaCurricular, CursosMalla, FiltrosMalla
from semestre.models import Semestre
from django.forms import formset_factory, inlineformset_factory
#from searchableselect.widgets import SearchableSelect
import json


"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""
class MallaCreateForm(LoginRequiredMixin, forms.ModelForm):

	ayuda = forms.CharField(widget=SummernoteWidget(), label='Ayuda')

	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = MallaCurricular
		widgets = {'ayuda': SummernoteWidget()}
		fields = ['resolucion', 'ayuda']

class CursosMallaCreateForm(LoginRequiredMixin, forms.ModelForm):

	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = CursosMalla
		#widgets = {'prerequisitos': SearchableSelect(model='mallacurricular.CursosMalla', search_field='nombre', limit=10)}
		fields = ['curso', 'semestre', 'prerequisitos', 'formaciongeneral', 'corequisitos']

CursosMallaFormSet = inlineformset_factory(MallaCurricular, CursosMalla, form=CursosMallaCreateForm, can_delete=False, extra=1)


"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class MallaCreateView(LoginRequiredMixin, CreateView):

	def get(self, request, *args, **kwargs):
	    """Primero ponemos nuestro object como nulo, se debe tener en
	    cuenta que object se usa en la clase CreateView para crear el objeto"""
	    self.object = None
	    form = MallaCreateForm()
	    formset = CursosMallaFormSet()
	    forms = [form, formset]
	    programa = self.kwargs['epk']
	    semestres = Semestre.objects.filter(programa=programa).order_by('numero')
	    print(semestres)
	    context = {
		'section_title':'Nueva malla curricular',
		'form':form,
		'formset':formset ,
		'semestres':semestres}

	    return render(request, 'mallacurricular/mallacurricular_form.html', context)


	def post(self, request, *args, **kwargs):

	    form = MallaCreateForm(request.POST)
	    formset = CursosMallaFormSet(request.POST)


	    if form.is_valid() and formset.is_valid():
	    	return self.form_valid(form, formset)
	    else:
	    	return self.form_invalid(form, formset)


	def form_valid(self, form, formset):
		self.object = form.save()
		formset.instance = self.object
		formset.save()
		url = reverse('mallacurricular:listar')
		messages.info(self.request,'Nueva malla curricular creada con exito')
		return HttpResponseRedirect(url)


	def form_invalid(self, form, formset):
		context = {
		'section_title':'Nueva Malla',
		'form':form,
		'formset':formset }
		messages.info(self.request,'Hay errores en algun campo, revise el formulario')
		return render(self.request, 'mallacurricular/mallacurricular_form.html', context)

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class MallaUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = MallaCurricular
	template_name = 'mallacurricular/mallacurricular_form.html'
	form_class = MallaCreateForm
	success_url = reverse_lazy('malla:listar')

	def get(self, request, *args, **kwargs):
		self.object = self.get_object()
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		cursos = CursosMalla.objects.filter(malla=self.object).order_by('pk')
		cursos_data = []
		for cur in cursos:
			asignaturas = {'curso':cur.curso, 'semestre':cur.semestre}
			cursos_data.append(asignaturas)

		formset = CursosMallaFormSet(initial=cursos_data)

		return self.render_to_response(self.get_context_data(form=form, formset=formset))


	def post(self, request, *args, **kwargs):
		self.object = self.get_object()
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		formset = CursosMallaFormSet(request.POST)

		if (form.is_valid() and formset.is_valid()):
			return self.form_valid(form, formset)
		else:
			return self.form_invalid(form, formset)

	def form_valid(self, form, formset):
		self.object = form.save()
		formset.instance = self.object
		CursosMalla.objects().filter(malla=self.object).delete()
		formset.save()
		messages.info(self.request,'Malla curricular actualizada con exito')
		return HttpResponseRedirect(self.success_url)


	def form_invalid(self, form, formset):
		context = {
		'section_title':'Actualizar Malla',
		'form':form,
		'formset':formset,}
		messages.info(self.request,'Hay errores en algun campo, revise el formulario')
		return render(self.request, 'mallacurricular/mallacurricular_form.html', context)