# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.template import loader
from django.template import Context
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django import forms
from inicio.mixins import LoginRequiredMixin
from .models import Perfil, AmbitosPrograma, ProgramaEducativo
from programa.models import Programa
from scc.models import SCC, SCCEspecificas
from sccfacultad.models import SCCFacultad
from componente.models import Componente
from componentefacultad.models import ComponenteFacultad
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget
import json
from django.forms import formset_factory, inlineformset_factory, widgets, CheckboxSelectMultiple
from django.forms.models import modelformset_factory
from django.forms.widgets import CheckboxSelectMultiple


"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""
class PerfilCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""

	sccFacultad =forms.ModelMultipleChoiceField(queryset=SCCFacultad.objects.all(), label="SCC generales - Opción 1")
	#componentesfacultad =forms.ModelMultipleChoiceField(queryset=ComponenteFacultad.objects.all(), widget=forms.CheckboxSelectMultiple(), label=" ")
	descripcion = forms.CharField(widget=SummernoteWidget(), label='Descripción')
	#opciones = forms.ChoiceField(choices=opciones)
	leyendaambitos = forms.Textarea()
	#opciones = forms.ChoiceField(choices=opciones)
	leyendasccgenericas = forms.Textarea()
	#opciones = forms.ChoiceField(choices=opciones)
	leyendasccespecificas = forms.Textarea()


	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = Perfil
		widgets = {'descripcion': SummernoteWidget()}
		fields = ['programa', 'resolucion', 'descripcion', 'sccFacultad', 'leyendaambitos', 'leyendasccgenericas', 'leyendasccespecificas']

	"""def __init__(self, *args, **kwargs):
		self.request = kwargs.pop('request')
		super().__init__(*args, **kwargs)
		self.fields['programa'].queryset = Programa.objects.filter(unidadacademica=self.request.user.profesor.unidadAcademica)
	"""
class AmbitoForm(LoginRequiredMixin, forms.ModelForm):
	#programa = forms.CharField(label='Programa')
	#programa = forms.ModelChoiceField(queryset=Programa.objects.all())

	class Meta:
		model = AmbitosPrograma
		fields = ('titulo',)

AmbitoFormSet = inlineformset_factory(Perfil, AmbitosPrograma, form=AmbitoForm, can_delete=False, extra=9)


class SCCGeneralesForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""	


	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = SCC
		fields = ['titulo', 'accion', 'contenido', 'contexto', 'sccfacultad']

SCCGeneralesFormSet = inlineformset_factory(Perfil, SCC, form=SCCGeneralesForm, can_delete=False, extra=5)


class SCCEspecificasForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""	


	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = SCCEspecificas
		fields = ['titulo', 'accion', 'contenido', 'contexto', 'sccfacultad']

SCCEspecificasFormSet = inlineformset_factory(Perfil, SCCEspecificas, form=SCCEspecificasForm, can_delete=False, extra=15)

"""class ComponenteProgramaForm(LoginRequiredMixin, forms.ModelForm):
	Permite crear un formulario con los campos del facultad


	class Meta:
		Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario
		model = Componente
		fields = ['titulo', 'accion', 'contenido', 'contexto', 'descripcion', 'scc']

ComponenteFormSet = inlineformset_factory(SCC, Componente, form=ComponenteProgramaForm, can_delete=False, extra=1,  max_num=100, validate_max=True)
"""

class ProgramaEducativoCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""


	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = ProgramaEducativo
		fields = ['programa', 'documento']

"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class PerfilCreateView(LoginRequiredMixin, CreateView):

	model = Perfil
	template_name = 'perfil/perfil_form.html'
	form_class = PerfilCreateForm
	success_url = reverse_lazy('perfil:listar')


	def get(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		#form = PerfilCreateForm(request=request)
		form = PerfilCreateForm()
		formset = AmbitoFormSet()
		formsetDos = SCCGeneralesFormSet()
		formsetTres = SCCEspecificasFormSet()

		return self.render_to_response(self.get_context_data(form=form, formset=formset, formsetDos=formsetDos, formsetTres=formsetTres))


	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		#form = PerfilCreateForm(self.request.POST, request=request)
		form = PerfilCreateForm(self.request.POST)
		formset = AmbitoFormSet(self.request.POST)
		formsetDos = SCCGeneralesFormSet(self.request.POST)
		formsetTres = SCCEspecificasFormSet(self.request.POST)

		if (form.is_valid() and formset.is_valid() and formsetDos.is_valid() and formsetTres.is_valid()):
			return self.form_valid(form, formset, formsetDos, formsetTres)
		else:
			return self.form_invalid(form, formset, formsetDos, formsetTres)

	def form_valid(self, form, formset, formsetDos, formsetTres):
		self.object = form.save()
		formset.instance = self.object
		formset.save()
		formsetDos.instance = self.object
		formsetDos.save()
		formsetTres.instance = self.object
		formsetTres.save()
		url = reverse('perfil:listar')
		messages.info(self.request,'Nuevo perfil creado con exito')
		return HttpResponseRedirect(url)


	def form_invalid(self, form, formset, formsetDos, formsetTres):
		form = PerfilCreateForm(request=self.request)

		context = {
		'section_title':'Nuevo Perfil',
		'form':form,
		'formset':formset,
		'formsetDos':formsetDos,
		'formsetTres':formsetTres,}
		messages.info(self.request,'Hay errores en algun campo, revise el formulario')
		return render(self.request, 'perfil/perfil_form.html', context)



class PerfilUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de un perfil de egresado"""

	model = Perfil
	template_name = 'perfil/perfil_form.html'
	form_class = PerfilCreateForm
	success_url = reverse_lazy('perfil:listar')

	def get(self, request, *args, **kwargs):
		self.object = self.get_object()
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		ambitos = AmbitosPrograma.objects.filter(perfil=self.object).order_by('pk')
		competencias = SCC.objects.filter(perfil=self.object).order_by('pk')
		competenciasEspecificas = SCCEspecificas.objects.filter(perfil=self.object).order_by('pk')
		ambitos_data = []
		competencias_data = []
		competenciasEspecificas_data = []
		for ambito in ambitos:
			amb = {'titulo':ambito.titulo}
			ambitos_data.append(amb)

		for competencia in competencias:
			comp = {'titulo':competencia.titulo, 'accion':competencia.accion, 'contenido':competencia.contenido, 'contexto':competencia.contexto}
			competencias_data.append(comp)

		for competencia in competenciasEspecificas:
			comp = {'titulo':competencia.titulo, 'accion':competencia.accion, 'contenido':competencia.contenido, 'contexto':competencia.contexto}
			competenciasEspecificas_data.append(comp)

		formset = AmbitoFormSet(initial=ambitos_data)
		formsetDos = SCCGeneralesFormSet(initial=competencias_data)
		formsetTres = SCCEspecificasFormSet(initial=competenciasEspecificas_data)

		return self.render_to_response(self.get_context_data(form=form, formset=formset, formsetDos=formsetDos, formsetTres=formsetTres))


	def post(self, request, *args, **kwargs):
		self.object = self.get_object()
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		formset = AmbitoFormSet(request.POST)
		formsetDos = SCCGeneralesFormSet(request.POST)
		formsetTres = SCCEspecificasFormSet(request.POST)

		if (form.is_valid() and formset.is_valid() and formsetDos.is_valid() and formsetTres.is_valid()):
			return self.form_valid(form, formset, formsetDos, formsetTres)
		else:
			return self.form_invalid(form, formset, formsetDos, formsetTres)

	def form_valid(self, form, formset, formsetDos, formsetTres):
		self.object = form.save()
		formset.instance = self.object
		formsetDos.instance = self.object
		formsetTres.instance = self.object
		AmbitosPrograma.objects.filter(perfil=self.object).delete()
		SCC.objects.filter(perfil=self.object).delete()
		SCCEspecificas.objects.filter(perfil=self.object).delete()

		formset.save()
		formsetDos.save()
		formsetTres.save()
		messages.info(self.request,'Perfil actualizaco con exito')
		return HttpResponseRedirect(self.success_url)


	def form_invalid(self, form, formset, formsetDos, formsetTres):
		context = {
		'section_title':'Actualizar Perfil',
		'form':form,
		'formset':formset,
		'formsetDos':formsetDos,
		'formsetTres':formsetTres}
		messages.info(self.request,'Hay errores en algun campo, revise el formulario')
		return render(self.request, 'perfil/perfil_form.html', context)

"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class SCCFacultadListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = SCCFacultad
	context_object_name = 'sccsfacultad'
	template_name = 'perfil/perfil_form.html'

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		context = super(SCCFacultadListView,self).get_context_data(**kwargs)
		return context

class PerfilDeleteView(LoginRequiredMixin, DeleteView):
	"""Permite mientras se ejecuta contener el objeto persona sobre el que se esta 
	operando la vista"""
	
	model = Perfil
	template_name = 'perfil/perfil_confirm_delete.html'
	success_url = reverse_lazy('perfil:listar')

def ProgramaEducativoCreateView(request):
	if request.method == 'POST':
		form = ProgramaEducativoCreateForm(request.POST, request.FILES)
		if form.is_valid():
			programa = request.POST['programa']
			documento = request.FILES['documento']
			insert = ProgramaEducativo(programa=programa, documento=documento)
			insert.save()

			return reverse_lazy('programaeducativo:listarpep')
		else:
			messages.error(request, "Error al procesar el formulario")
	else:
		return reverse_lazy('programaeducativo:listarpep')


"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class ProgramaEducativoCreateView(LoginRequiredMixin, CreateView):
	form_class = ProgramaEducativoCreateForm
	model = ProgramaEducativo


	def get_context_data(self,**kwargs):
		context = super(ProgramaEducativoCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nuevo Programa Educativo del Programa'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
	
		messages.info(self.request,"El programa educativo del programa ha sido creado con exito")
		return reverse_lazy('perfil:listarpep')

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class ProgramaEducativoUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = ProgramaEducativo
	fields = ['programa', 'documento']
	success_url = reverse_lazy('perfil:listarpep')
	

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(ProgramaEducativoUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Programa Educativo del Programa'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"el programa educativo del programa ha sido actualizado con exito")
		return reverse_lazy('perfil:listarpep')

class ProgramaEducativoDeleteView(LoginRequiredMixin, DeleteView):
	"""Permite mientras se ejecuta contener el objeto persona sobre el que se esta 
	operando la vista"""
	
	model = ProgramaEducativo
	template_name = 'perfil/programaeducativo_confirm_delete.html'
	success_url = reverse_lazy('perfil:listarpep')