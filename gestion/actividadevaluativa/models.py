# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class ActividadEvaluacion(models.Model):
	"""Permite definir un listado de SCC"""

	#Nombre de la actividad
	nombre = models.CharField(max_length=200)
	#Descripcion de la actividad
	descripcion = models.CharField(max_length=200)
	#Curso


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Actividades de Evaluación"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.nombre

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.nombre
