 # -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import SemestreCreateView, SemestreUpdateView, SemestreDeleteView
from .views import SemestreListView

app_name = 'semestre'

urlpatterns = [

	path('semestre/crear/', SemestreCreateView.as_view(), name='crear'),
	path('semestre/listar/', SemestreListView.as_view(), name='listar'),
	path('semestre/actualizar/<int:pk>', SemestreUpdateView.as_view(), name='actualizar'),
	path('semestre/eliminar/<int:pk>', SemestreDeleteView.as_view(), name='eliminar'),
]