# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Programa, SCCFacultadPrograma

"""Permite administrar la visualización de los datos de Programa en la base de datos del
	sitio de administración"""
class AdminPrograma(admin.ModelAdmin):
	"""Permite establecer la información de los datos id, nombre, codigo, unidadacademica y tipo del 
	modelo Programa que se mostrarán en el sitio de administración"""
	list_display = ('id', 'nombre','codigo', 'unidadacademica', 'nivel', 'jornada')
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Programa 
	en el sitio de administración"""
	search_fields = ('nombre', 'codigo')

#Permite registrar las clases Programa y AdminPrograma
admin.site.register(Programa, AdminPrograma)

"""Permite administrar la visualización de los datos de Perfil en la base de datos del
	sitio de administración"""
class AdminSCCFacultadPrograma(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Perfil que se mostrarán en el sitio de administración"""
	list_display = ('id', 'sccsfacultad', 'programa',)
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Perfil en el sitio 
	de administración"""
	search_fields = ('id', 'programa')

#Permite registrar las clases Perfil y AdminPerfil
admin.site.register(SCCFacultadPrograma, AdminSCCFacultadPrograma)
