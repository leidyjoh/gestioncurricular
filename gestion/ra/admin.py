# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import ResultadoAprendizaje

"""Permite administrar la visualización de los datos de Programa en la base de datos del
	sitio de administración"""
class AdminResultadoAprendizaje(admin.ModelAdmin):
	"""Permite establecer la información de los datos id, nombre, codigo, unidadacademica y tipo del 
	modelo Programa que se mostrarán en el sitio de administración"""
	list_display = ('id', 'numero','accion', 'contenido', 'contexto', 'finalidad', 'descripcion', 'nivelAprendizaje', 'programa', 'curso', 'porcentaje', 'posiblesActividades')
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Programa 
	en el sitio de administración"""
	search_fields = ('nombre', 'descripcion')

#Permite registrar las clases Programa y AdminPrograma
admin.site.register(ResultadoAprendizaje, AdminResultadoAprendizaje)