# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
from .models import MallaCurricular, CursosMalla


"""Permite administrar la visualización de los datos de Perfil en la base de datos del
	sitio de administración"""
class AdminMallaCurricular(SummernoteModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Perfil que se mostrarán en el sitio de administración"""
	list_display = ('id', 'programa', 'resolucion', 'ayuda',)	
	summernote_fields = ('ayuda',)
	"""Permite establecer el parametro de busqueda nombre de la tabla Perfil en el sitio 
	de administración"""
	search_fields = ('id', 'programa')

#Permite registrar las clases Perfil y AdminPerfil
admin.site.register(MallaCurricular, AdminMallaCurricular)


class AdminCursosMalla(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Perfil que se mostrarán en el sitio de administración"""
	list_display = ('id', 'malla', 'curso', 'semestre', 'get_prerequisitos', 'get_formacion', 'get_corequisitos')	
	"""Permite establecer el parametro de busqueda nombre de la tabla Perfil en el sitio 
	de administración"""
	search_fields = ('id', 'semestre')
	


#Permite registrar las clases Perfil y AdminPerfil
admin.site.register(CursosMalla, AdminCursosMalla)