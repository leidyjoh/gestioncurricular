# Generated by Django 2.0.5 on 2021-06-11 13:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ra', '0003_auto_20210611_0739'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resultadoaprendizaje',
            name='accion',
            field=models.CharField(blank=True, default='', max_length=200, null=True, verbose_name='Acción'),
        ),
        migrations.AlterField(
            model_name='resultadoaprendizaje',
            name='contenido',
            field=models.CharField(blank=True, default='', max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='resultadoaprendizaje',
            name='contexto',
            field=models.CharField(blank=True, default='', max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='resultadoaprendizaje',
            name='finalidad',
            field=models.CharField(blank=True, default='', max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='resultadoaprendizaje',
            name='programa',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='programa.Programa'),
        ),
    ]
