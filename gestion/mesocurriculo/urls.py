 # -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import MesoCreateView, MesoCreateTablaView, MesoCreateArbolView, MesoCreateListaView

app_name = 'mesocurriculo'

urlpatterns = [

	path('meso/crear/', MesoCreateView.as_view(), name='crear'),
	path('meso/creartabla/', MesoCreateTablaView.as_view(), name='crear-tabla'),
	path('meso/creararbol/', MesoCreateArbolView.as_view(), name='crear-arbol'),
	path('meso/crearlista/', MesoCreateListaView.as_view(), name='crear-lista'),
	#path('scc/listar/', FacultadListView.as_view(), name='listar'),
]