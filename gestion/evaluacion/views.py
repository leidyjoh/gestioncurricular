# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from mallacurricular.models import CursosMalla
from indicadorlogro.models import IndicadorLogro
from ra.models import ResultadoAprendizaje
from .models import Evaluacion
from inicio.mixins import LoginRequiredMixin
from django.shortcuts import render
from itertools import chain
from operator import attrgetter


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class EvaluacionListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""
	model = IndicadorLogro
	context_object_name = 'ras'
	template_name = 'evaluacion/ra_list.html'


	def get_queryset(self):
		#ra = ResultadoAprendizaje.objects.all()
		#il = IndicadorLogro.objects.all()
		#queryset = sorted(chain(ra,il), key=attrgetter('id'),)
		Indicador= IndicadorLogro.objects.all()
		return Indicador.values("descripcion", "resultadoAprendizaje__descripcion", "resultadoAprendizaje__porcentaje")



"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class EvaluacionIndicadorLogroListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""
		#ventasproducto = ventas.values("articulo").annotate(cuantos=Sum('totalProducto')).order_by(Coalesce('cuantos', 'cuantos').desc())

	model = IndicadorLogro
	context_object_name = 'il'
	template_name = 'evaluacion/ra_list.html'

	#def get_queryset(self):
		#return ComponenteFacultad.values("descripcion").annotate()

	def get_queryset(self):
		return IndicadorLogro.objects.all()