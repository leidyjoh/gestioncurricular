# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from programa.models import Programa, SCCFacultadPrograma
from facultad.models import Facultad
from sccfacultad.models import SCCFacultad
from perfil.models import Perfil


#Tipo de scc
GENERICA = 'Genérica'
ESPECIFICA = 'Específica'

tipo_scc = (
	(GENERICA, 'Genérica'),
	(ESPECIFICA, 'Específica'),
)
#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class SCC(models.Model):
	"""Permite definir un listado de SCC"""

	#Titulo de la scc
	titulo = models.CharField(max_length=200, verbose_name= ('Título: '))
	#Accion de la scc
	accion = models.CharField(max_length=200, verbose_name= ('Acción: '))
	#Contenido de la scc
	contenido = models.CharField(max_length=200, verbose_name= ('Contenido: '))
	#Contexto de la scc
	contexto = models.CharField(max_length=200, verbose_name= ('Contexto: '))
	#Descripción de la scc
	descripcion = models.CharField(max_length=200, verbose_name= ('Descripción: '))
	#Programa a la que pertenece la scc
	perfil = models.ForeignKey(Perfil, on_delete=models.CASCADE)
	#SCC de la facultad
	sccfacultad = models.ForeignKey(SCCFacultad, on_delete=models.CASCADE, verbose_name= ('SCC Facultad a la que apunta'), null=True, blank=True)
	#tipo de scc
	#tipo = models.CharField(max_length=50, choices=tipo_scc, default=GENERICA)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['titulo']
		verbose_name_plural = "SCCS"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return str(self.id) +". " + self.titulo

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.id) + ". "+ self.titulo


	def save(self, *args, **kwargs):
		self.descripcion = self.accion +" "+ self.contenido + " "+ self.contexto
		super(SCC, self).save(*args,**kwargs)


#Tipo de scc
GENERICA = 'Genérica'
ESPECIFICA = 'Específica'

tipo_scc = (
	(GENERICA, 'Genérica'),
	(ESPECIFICA, 'Específica'),
)

#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class SCCEspecificas(models.Model):
	"""Permite definir un listado de SCC"""

	#Titulo de la scc
	titulo = models.CharField(max_length=200, verbose_name= ('Título: '))
	#Accion de la scc
	accion = models.CharField(max_length=200, null=True, blank=True, verbose_name= ('Acción: '))
	#Contenido de la scc
	contenido = models.CharField(max_length=200, null=True, blank=True, verbose_name= ('Contenido: '))
	#Contexto de la scc
	contexto = models.CharField(max_length=200, null=True, blank=True, verbose_name= ('Contexto: '))
	#Programa a la que pertenece la scc
	perfil = models.ForeignKey(Perfil, on_delete=models.CASCADE)
	#SCC de la facultad
	sccfacultad = models.ManyToManyField(SCCFacultad, verbose_name= ('SCC Facultad a la que apunta'), null=True, blank=True)
	#sccfacultad = models.ForeignKey(SCCFacultadPrograma, on_delete=models.CASCADE, verbose_name= ('SCC Facultad'))
	#tipo de scc
	#tipo = models.CharField(max_length=50, choices=tipo_scc, default=GENERICA)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['titulo']
		verbose_name_plural = "SCC Especificas"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return str(self.id) +". " + self.titulo

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.id) + ". "+ self.titulo


	def save(self, *args, **kwargs):
		self.descripcion = self.accion +" "+ self.contenido + " "+ self.contexto
		super(SCCEspecificas, self).save(*args,**kwargs)

	def get_sccFacultad(self):
		return "* \n".join([p.titulo for p in self.sccfacultad.all()])
