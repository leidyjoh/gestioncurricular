# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from programa.models import Programa
from sccfacultad.models import SCCFacultad

#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla UnidadAcademica en base de datos"""
class Perfil(models.Model):
	"""Permite definir un listado de UnidadesAcademicas"""

	#Programa al que pertenece
	programa = models.OneToOneField(Programa, on_delete=models.CASCADE)
	#Resolución
	resolucion = models.CharField(max_length=200, verbose_name= ('Resolución'))
	#Descripcion del perfil de egresado
	descripcion = models.TextField(max_length=58000)
	#Descripcion del perfil de egresado
	leyendaambitos = models.TextField(max_length=2000, null=True, blank=True, verbose_name = ('Introducción a los ámbitos o perfiles ocupacionales'))
	#Descripcion del perfil de egresado
	leyendasccgenericas = models.TextField(max_length=2000, null=True, blank=True, verbose_name = ('Introducción a las SCC generales'))
	#Descripcion del perfil de egresado
	leyendasccespecificas = models.TextField(max_length=2000,  null=True, blank=True, verbose_name = ('Introducción a las SCC específicas'))
	#SCCFacultad del perfil de egresado
	#SCCFacultad del perfil de egresado
	sccFacultad = models.ManyToManyField(SCCFacultad, null=True, blank=True,  verbose_name = ('SCC generales - Opción 1'))

	

	"""Permite ordenar la lista de perfiles por programa y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['programa']
		verbose_name_plural = "Perfiles"

	#Permite determinar una representación en string del objeto Perfil
	def __str__(self):
		return str(self.programa)

	"""Permite determinar una representación en string para el objeto Perfil(Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.programa)

	def get_sccFacultad(self):
		return "* \n".join([p.titulo for p in self.sccFacultad.all()])

#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla UnidadAcademica en base de datos"""
class AmbitosPrograma(models.Model):
	"""Permite definir un listado de UnidadesAcademicas"""

	#titulo del ambito
	titulo = models.CharField(max_length=1200, verbose_name = ('Nombre del ámbito o perfil ocupacional'))
	#Perfil al que pertenece
	perfil = models.ForeignKey(Perfil, on_delete=models.CASCADE)
	

	"""Permite ordenar la lista de perfiles por programa y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['perfil']
		verbose_name_plural = "Ambitos del Programa"

	#Permite determinar una representación en string del objeto Perfil
	def __str__(self):
		return str(self.titulo)

	"""Permite determinar una representación en string para el objeto Perfil(Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.titulo)

class ProgramaEducativo(models.Model):
	
	#programa
	programa = models.ForeignKey(Programa, on_delete=models.CASCADE)
	#documento
	documento = models.FileField(upload_to="imagenes/documento")


	class Meta:
		ordering = ['programa']
		verbose_name_plural = "Programa Educativo del Programa"

	#Permite determinar una representación en string del objeto Perfil
	def __str__(self):
		return str(self.programa.nombre)

	"""Permite determinar una representación en string para el objeto Perfil(Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.programa.nombre)

#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class Competencias(models.Model):

	"""Permite definir un listado de SCC"""
	titulo = models.CharField(max_length=200, verbose_name= ('Título'))
	#facultad de scc
	perfil = models.ForeignKey(Perfil, on_delete=models.CASCADE)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['perfil']
		verbose_name_plural = "Competencias"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return str(self.titulo)

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.titulo)