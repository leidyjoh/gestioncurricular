# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from mallacurricular.models import CursosMalla
from usuario.models import Profesor, Usuario
from ra.models import ResultadoAprendizaje
from actividadaprendizaje.models import ActividadAprendizaje
from actividadevaluativa.models import ActividadEvaluacion

tipo_validable = (
	(True, 'Si'),
	(False, 'No'),
)

tipo_habilitable = (
	(True, 'Si'),
	(False, 'No'),
)
#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class DiseñoCurso(models.Model):
	"""Permite definir un listado de SCC"""

	#Docente que realiza el diseño
	docente = models.ForeignKey(Profesor, on_delete=models.CASCADE)
	#Curso del diseño
	curso = models.ForeignKey(CursosMalla, on_delete=models.CASCADE, related_name="Cursosdiseño")


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['docente']
		verbose_name_plural = "Asignación Diseños Cursos"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return str(self.docente)

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.docente)

class DiseñoActApRA(models.Model):
	"""Permite definir un listado de SCC"""

	#Diseño del curso
	diseñoCurso = models.ForeignKey(DiseñoCurso, on_delete=models.CASCADE, related_name='diseñarcurso')
	#Actividad de aprendizaje
	actividadAprendizaje = models.ForeignKey(ActividadAprendizaje, on_delete=models.CASCADE)
	#Resultado de aprendizaje
	#resultadoAprendizaje = models.ForeignKey(ResultadoAprendizaje, on_delete=models.CASCADE)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['diseñoCurso']
		verbose_name_plural = "Diseños Cursos-ActividadAprendizaje-RA"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.diseñocurso

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.diseñocurso


class DiseñoActEvaRO(models.Model):
	"""Permite definir un listado de SCC"""

	#Diseño del curso
	diseñoCurso = models.ForeignKey(DiseñoCurso, on_delete=models.CASCADE, related_name='Dcurso')
	#Actividad de evaluación
	actividadEvaluacion = models.ForeignKey(ActividadEvaluacion, on_delete=models.CASCADE, related_name='actividadE')
	#Resultado de aprendizaje
	indicadorLogro = models.ForeignKey(ActividadEvaluacion, on_delete=models.CASCADE)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['diseñoCurso']
		verbose_name_plural = "Diseños Cursos-ActividadEvaluacion-RO"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.diseñocurso

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.diseñocurso


class DiseñoActEvaRO(models.Model):
	"""Permite definir un listado de SCC"""

	#Diseño del curso
	diseñoCurso = models.ForeignKey(DiseñoCurso, on_delete=models.CASCADE, related_name='Dcurso')
	#Actividad de evaluación
	actividadEvaluacion = models.ForeignKey(ActividadEvaluacion, on_delete=models.CASCADE, related_name='actividadE')
	#Resultado de aprendizaje
	indicadorLogro = models.ForeignKey(ActividadEvaluacion, on_delete=models.CASCADE)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['diseñoCurso']
		verbose_name_plural = "Diseños Cursos-ActividadEvaluacion-RO"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.diseñocurso

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.diseñocurso

class Microcurriculo(models.Model):
	"""Permite definir un listado de SCC"""

	curso = models.ForeignKey(CursosMalla, on_delete=models.CASCADE, null=True, blank=True)
	#Diseño del curso
	ejes = models.TextField(max_length=58000, verbose_name='Ejes temáticos')
	#Resultado de aprendizaje
	validable = models.BooleanField(choices=tipo_validable, verbose_name='Validable', default=False)
	#Resultado de aprendizaje
	habilitable = models.BooleanField(choices=tipo_habilitable, verbose_name='Halitable', default=False)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['curso']
		verbose_name_plural = "Microcurriculos"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.curso

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.curso
