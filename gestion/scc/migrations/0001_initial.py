# Generated by Django 2.0.5 on 2021-06-10 06:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('perfil', '0001_initial'),
        ('sccfacultad', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SCC',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=200, verbose_name='Título: ')),
                ('accion', models.CharField(max_length=200, verbose_name='Acción: ')),
                ('contenido', models.CharField(max_length=200, verbose_name='Contenido: ')),
                ('contexto', models.CharField(max_length=200, verbose_name='Contexto: ')),
                ('descripcion', models.CharField(max_length=200, verbose_name='Descripción: ')),
                ('perfil', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='perfil.Perfil')),
                ('sccfacultad', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='sccfacultad.SCCFacultad', verbose_name='SCC Facultad a la que apunta')),
            ],
            options={
                'verbose_name_plural': 'SCCS',
                'ordering': ['titulo'],
            },
        ),
        migrations.CreateModel(
            name='SCCEspecificas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=200, verbose_name='Título: ')),
                ('accion', models.CharField(blank=True, max_length=200, null=True, verbose_name='Acción: ')),
                ('contenido', models.CharField(blank=True, max_length=200, null=True, verbose_name='Contenido: ')),
                ('contexto', models.CharField(blank=True, max_length=200, null=True, verbose_name='Contexto: ')),
                ('perfil', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='perfil.Perfil')),
                ('sccfacultad', models.ManyToManyField(blank=True, null=True, to='sccfacultad.SCCFacultad', verbose_name='SCC Facultad a la que apunta')),
            ],
            options={
                'verbose_name_plural': 'SCC Especificas',
                'ordering': ['titulo'],
            },
        ),
    ]
