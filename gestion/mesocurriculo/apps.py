from django.apps import AppConfig


class MesocurriculoConfig(AppConfig):
    name = 'mesocurriculo'
