# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView
from django.template import loader
from django.template import Context
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse
from django.contrib import messages
from django import forms
from inicio.mixins import LoginRequiredMixin
from .models import UnidadAcademica
import json

"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""
class UnidadAcademicaCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""

	nombre = forms.CharField(label='Nombre')
	

	def clean_nombre(self):
		"""Permite validar que el campo nombre_propietario sólo contenga letras[A-Z],[a-z]"""

		n=0
		nombre = self.cleaned_data['nombre']
		if nombre.isalpha():
			return nombre
		else:
			if " " in nombre:
				separar=nombre.split(" ")
				for c in range(len(separar)):
					if separar[c].isalpha():
						n=n+1
					else:
						n=-1
						break
				if n>0:
					return nombre
				else:
					if n<0:
						raise forms.ValidationError('El nombre de la unidad académica solo puede contener letras')
			else:
				raise forms.ValidationError("El nombre de la unidad académica solo puede contener letras")	


	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = UnidadAcademica
		fields = ['nombre', 'facultad']

"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class UnidadAcademicaCreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = UnidadAcademicaCreateForm
	model = UnidadAcademica

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para crear un VehículoEspecial"""
		context = super(UnidadAcademicaCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nueva Unidad Académica'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"La unidad académica ha sido creada con exito")
		return reverse_lazy('unidadacademica:listar')

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class UnidadAcademicaUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = UnidadAcademica
	fields = ['nombre', 'facultad']
	success_url = reverse_lazy('unidadacademica:listar')

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(UnidadAcademicaUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Unidad Académica'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"La unidad académica ha sido actualizada con exito")
		return reverse_lazy('unidadacademica:listar')

class UnidadAcademicaDeleteView(LoginRequiredMixin, DeleteView):
	"""Permite mientras se ejecuta contener el objeto persona sobre el que se esta 
	operando la vista"""
	
	model = UnidadAcademica
	template_name = 'unidadacademica/unidadacademica_confirm_delete.html'
	success_url = reverse_lazy('unidadacademica:listar')
