 # -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import FacultadCreateView, FacultadUpdateView, CicloCreateView, CicloUpdateView, CicloDeleteView, TipoAsignaturaCreateView, TipoAsignaturaUpdateView, TipoAsignaturaDeleteView, FormacionCreateView, FormacionUpdateView, FormacionDeleteView, NivelAprendizajeCreateView, NivelAprendizajeUpdateView, NivelAprendizajeDeleteView
from .views import FacultadListView, CicloListView, TipoAsignaturaListView, FormacionListView, NivelAprendizajeListView

app_name = 'facultad'

urlpatterns = [

	path('facultad/crear/', FacultadCreateView.as_view(), name='crear'),
	path('facultad/listar/', FacultadListView.as_view(), name='listar'),
	path('facultad/actualizar/<int:pk>', FacultadUpdateView.as_view(), name='actualizar'),
	path('facultad/crearciclo/', CicloCreateView.as_view(), name='crearciclo'),
	path('facultad/listarciclo/', CicloListView.as_view(), name='listarciclo'),
	path('facultad/actualizarciclo/<int:pk>', CicloUpdateView.as_view(), name='actualizarciclo'),
	path('facultad/eliminarciclo/<int:pk>', CicloDeleteView.as_view(), name='eliminarciclo'),
	path('facultad/creartipoasignatura/', TipoAsignaturaCreateView.as_view(), name='creartipoasignatura'),
	path('facultad/listartipoasignatura/', TipoAsignaturaListView.as_view(), name='listartipoasignatura'),
	path('facultad/actualizartipoasignatura/<int:pk>', TipoAsignaturaUpdateView.as_view(), name='actualizartipoasignatura'),
	path('facultad/eliminartipoasignatura/<int:pk>', TipoAsignaturaDeleteView.as_view(), name='eliminartipoasignatura'),
	path('facultad/crearformacion/', FormacionCreateView.as_view(), name='crearformacion'),
	path('facultad/listarformacion/', FormacionListView.as_view(), name='listarformacion'),
	path('facultad/actualizarformacion/<int:pk>', FormacionUpdateView.as_view(), name='actualizarformacion'),
	path('facultad/eliminarformacion/<int:pk>', FormacionDeleteView.as_view(), name='eliminarformacion'),
		path('facultad/crearnivelaprendizaje/', NivelAprendizajeCreateView.as_view(), name='crearnivelaprendizaje'),
	path('facultad/listarnivelaprendizaje/', NivelAprendizajeListView.as_view(), name='listarnivelaprendizaje'),
	path('facultad/actualizarnivelaprendizaje/<int:pk>', NivelAprendizajeUpdateView.as_view(), name='actualizarnivelaprendizaje'),
	path('facultad/eliminarnivelaprendizaje/<int:pk>', NivelAprendizajeDeleteView.as_view(), name='eliminarnivelaprendizaje'),
]