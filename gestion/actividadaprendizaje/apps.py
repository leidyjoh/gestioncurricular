from django.apps import AppConfig


class ActividadaprendizajeConfig(AppConfig):
    name = 'actividadaprendizaje'
