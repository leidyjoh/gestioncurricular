 # -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .views import EvaluacionListView

app_name = 'evaluacion'

urlpatterns = [

	path('evaluacion/listar/<int:pk>', EvaluacionListView.as_view(), name='listarRa'),
]