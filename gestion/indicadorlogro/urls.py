# -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import IndicadorLogroCreateView, IndicadorLogroUpdateView
from .views import IndicadorLogroListView

app_name = 'indicadorlogro'

urlpatterns = [

	path('indicadorlogro/crear/', IndicadorLogroCreateView.as_view(), name='crear'),
	path('indicadorlogro/listar/', IndicadorLogroListView.as_view(), name='listar'),
	path('indicadorlogro/actualizar/<int:pk>', IndicadorLogroUpdateView.as_view(), name='actualizar'),
]