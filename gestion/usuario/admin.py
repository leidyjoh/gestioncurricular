# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Profesor, Estudiante


"""Permite administrar la visualización de los datos de Programa en la base de datos del
	sitio de administración"""
class AdminProfesor(admin.ModelAdmin):
	"""Permite establecer la información de los datos id, nombre, codigo, unidadacademica y tipo del 
	modelo Programa que se mostrarán en el sitio de administración"""
	list_display = ('id', 'user', 'facultad', 'unidadAcademica', 'programa', 'identificacion', 'estado', 'imagen', 'cargoAdministrativo')

	"""Permite establecer el parametro de busqueda nombre de la tabla Programa 
	en el sitio de administración"""
	search_fields = ('user', 'nombre')

#Permite registrar las clases Programa y AdminPrograma
admin.site.register(Profesor, AdminProfesor)

"""Permite administrar la visualización de los datos de Programa en la base de datos del
	sitio de administración"""
class AdminEstudiante(admin.ModelAdmin):
	"""Permite establecer la información de los datos id, nombre, codigo, unidadacademica y tipo del 
	modelo Programa que se mostrarán en el sitio de administración"""
	list_display = ('id', 'user','programa', 'identificacion', 'estado', 'imagen', 'codigo')

	"""Permite establecer el parametro de busqueda nombre de la tabla Programa 
	en el sitio de administración"""
	search_fields = ('user', 'nombre')

#Permite registrar las clases Programa y AdminPrograma
admin.site.register(Estudiante, AdminEstudiante)
