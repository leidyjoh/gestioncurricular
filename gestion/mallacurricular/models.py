# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from curso.models import Curso, Area
from semestre.models import Semestre
from facultad.models import CicloAsignatura, TipoAsignatura, FormacionGeneral
from programa.models import Programa
from smart_selects.db_fields import ChainedForeignKey
from smart_selects.db_fields import GroupedForeignKey
from django.db.models.query import QuerySet
from django_group_by import GroupByMixin


#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class MallaCurricular(models.Model):
	"""Permite definir un listado de Mallas"""

	#Programa al que pertenece
	programa = models.OneToOneField(Programa, on_delete=models.CASCADE, unique=True)
	#Resolución
	resolucion = models.CharField(max_length=200, verbose_name= ('Resolución'))
	#Descripción
	ayuda = models.TextField(max_length=58000)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['programa']
		verbose_name_plural = "Mallas Curriculares"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.programa.nombre + " - " +self.programa.jornada

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.programa.nombre + " - " +self.programa.jornada


#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class CursosMalla(models.Model):
	"""Permite definir un listado de Mallas"""
	#Malla
	malla = models.ForeignKey(MallaCurricular, on_delete=models.CASCADE)
	#Escuela
	curso = models.ForeignKey(Curso, on_delete=models.CASCADE, related_name="Cursosdelamalla")
	#curso = ChainedForeignKey(Curso, chained_field='curso', chained_model_field='malla__programa__unidadacademica', show_all=False, auto_choose=True, sort=True, related_name="malla", verbose_name = ('Curso: '))
	#Semestre
	semestre = models.ForeignKey(Semestre, on_delete=models.CASCADE)
	#Prerequisitos
	prerequisitos = models.ManyToManyField(Curso, blank=True, verbose_name= ('Prerrequisitos'), related_name="prerequisitos")
	#SCC a la que tributa
	corequisitos = models.ManyToManyField(Curso, blank=True, verbose_name= ('Correquisitos'))
	#ra = models.ManyToManyField(ResultadoAprendizaje, blank=True, null=True, verbose_name= ('RAs'))
	#SCC a la que tributa
	formaciongeneral = models.ManyToManyField(FormacionGeneral, blank=True, null=True, verbose_name= ('Formación General'))

	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['semestre']
		verbose_name_plural = "Cursos Malla Curricular"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return str(self.curso.nombre)

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.curso.nombre)

	def get_corequisitos(self):
		return "\n * ".join([p.nombre for p in self.corequisitos.all()])

	def get_prerequisitos(self):
		return "\n * ".join([p.nombre for p in self.prerequisitos.all()])

	def get_formacion(self):
		return "\n * ".join([p.nombre for p in self.formaciongeneral.all()])


#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class FiltrosMalla(models.Model):
	"""Permite definir un listado de Mallas"""
	#Ciclos
	ciclo = models.ForeignKey(CicloAsignatura, on_delete=models.CASCADE)
	#Áreas
	area = models.ForeignKey(Area, on_delete=models.CASCADE)
	#Ásignaturas
	asignatura = models.ForeignKey(TipoAsignatura, on_delete=models.CASCADE)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['ciclo']
		verbose_name_plural = "Filtros Malla"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.programa.nombre

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.programa.nombre


