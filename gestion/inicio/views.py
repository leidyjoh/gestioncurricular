# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, render_to_response
from django.views.generic import TemplateView
from django.contrib.auth.forms import PasswordResetForm
from django.db.models import Count, Sum
from django.contrib.auth.views import password_reset, password_reset_confirm
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.db.models.functions import Coalesce
from facultad.models import Facultad
from unidadacademica.models import UnidadAcademica
from programa.models import Programa
from scc.models import SCC
from sccfacultad.models import SCCFacultad
from usuario.models import ADMINISTRATIVO, NINGUNO, VICEDECANO, DIRECTORESCUELA, Profesor, DIRECTORPROGRAMA
from unidadacademica.models import UnidadAcademica
import json
from django.core.serializers.json import DjangoJSONEncoder
from datetime import datetime, date

#404: página no encontrada
def pag_404_not_found(request, exception, template_name="error/404.html"):
    response = render_to_response("inicio/pages-404.html")
    response.status_code=404
    return response
 
#500: error en el servidor
def pag_500_error_server(request, exception,template_name="error/500.html"):
    response = render_to_response("inicio/pages-500.html")
    response.status_code=500
    return response

class Login(TemplateView):

	#Cuando la peticion es tipo GET, se muestra el template de login
	def get(self,request,*args,**kwargs):
		#Si el usuario esta autenticado, se le muestra su perfil
		if request.user.is_authenticated and not request.user.is_staff:
			return self.get_user_template(request)
		#En caso de que no este autenticado, se le muestra el formulario de login
		else:
			return render(request, 'inicio/contenido_login.html')

	#Cunado la peticion es tipo POST se hace el proceso de login con la informacion del formulario de login
	def post(self,request,*args,**kwargs):
		username = request.POST.get('username')
		password = request.POST.get('password')

		#Usando el la funcion authenticate, obtenemos el usuario que corresponde con los datos
		#pasados como argumentos
		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				login(request,user)

				#Retornamos una respuesta con el perfil del usuario
				return self.get_user_template(request)
			else:
				context = {'message':'Su usuario no esta activo'}
				return render(request, 'inicio/contenido_login.html', context)
		else:
			context = {'message':'Nombre de usuario o contraseña invalidos'}
			return render(request, 'inicio/contenido_login.html', context)


	def get_user_template(self,request):

		if request.user.profesor.cargoAdministrativo == ADMINISTRATIVO:
			num_facultades = Facultad.objects.all().count()
			num_escuelas = UnidadAcademica.objects.all().count()
			num_programas = Programa.objects.all().count()

			context = {
				'num_facultades':num_facultades,
				'num_escuelas':num_escuelas,
				'num_programas':num_programas
				}
			return render(request, 'cuenta/perfil_administrador.html', context)


		elif request.user.profesor.cargoAdministrativo == VICEDECANO:
			num_escuelas = UnidadAcademica.objects.all().count()
			num_programas = Programa.objects.all().count()
			num_scc_facultad = SCCFacultad.objects.all().count()

			context = {
				'num_escuelas':num_escuelas,
				'num_programas':num_programas,
				'num_scc_facultad':num_scc_facultad,
				}
			return render(request, 'cuenta/perfil_facultad.html', context)

		elif request.user.profesor.cargoAdministrativo == DIRECTORESCUELA:
			num_escuelas = UnidadAcademica.objects.all().count()
			num_programas = Programa.objects.all().count()
			num_scc_facultad = SCCFacultad.objects.all().count()

			context = {
				'num_escuelas':num_escuelas,
				'num_programas':num_programas,
				'num_scc_facultad':num_scc_facultad,
				}
			return render(request, 'cuenta/perfil_director_escuela.html', context)

		elif request.user.profesor.cargoAdministrativo == DIRECTORPROGRAMA:
			num_escuelas = UnidadAcademica.objects.all().count()
			num_programas = Programa.objects.all().count()
			num_scc_facultad = SCC.objects.all().count()

			context = {
				'num_escuelas':num_escuelas,
				'num_programas':num_programas,
				'num_scc_facultad':num_scc_facultad,
				}

			return render(request, 'cuenta/perfil_director_programa.html', context)

		elif request.user.profesor.cargoAdministrativo == NINGUNO:
			num_escuelas = UnidadAcademica.objects.all().count()
			num_programas = Programa.objects.all().count()
			num_scc_facultad = SCC.objects.all().count()

			context = {
				'num_escuelas':num_escuelas,
				'num_programas':num_programas,
				'num_scc_facultad':num_scc_facultad,
				}

			return render(request, 'cuenta/perfil_docente.html', context)


class Logout(TemplateView):

	def dispatch(self,request,*args,**kwargs):
		logout(request)
		context = {}
		return render(request, 'inicio/contenido_login.html', context)



class Iniciar(TemplateView):

	def get(self,request,*args,**kwargs):
		context = {}
		return render(request, 'inicio/pagina_inicio.html', context)


