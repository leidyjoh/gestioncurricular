# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from mallacurricular.models import CursosMalla
from semestre.models import Semestre
from programa.models import Programa
from perfil.models import Competencias
from sccfacultad.models import SCCFacultad
from scc.models import SCC, SCCEspecificas
from smart_selects.db_fields import ChainedForeignKey, GroupedForeignKey


#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class MatrizTributacion(models.Model):
	"""Permite definir un listado de Mallas"""

	#Programa al que pertenece
	programa = models.OneToOneField(Programa, on_delete=models.CASCADE, unique=True)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['programa']
		verbose_name_plural = "Matriz de Tributación"

	def get_scc(self):
		return "\n -".join([p.titulo for p in self.scc.all()])

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return str(self.programa) if self.programa else ''

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.programa) if self.programa else ''


class CursosTributacionMatriz(models.Model):
	"""Permite definir un listado de Mallas"""

	#Matriz
	matriz = models.ForeignKey(MatrizTributacion, on_delete=models.CASCADE)
	#SCC
	scc = models.ForeignKey(Competencias, on_delete=models.CASCADE, related_name="cursoM")
	#Curso
	curso = models.ManyToManyField(CursosMalla, blank=True, null=True)

	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['scc']
		verbose_name_plural = "Cursos Tributación Matriz"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.scc.titulo

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.scc.titulo


	def get_cursos(self):
		return "* \n".join([p.curso.nombre for p in self.curso.all()])
