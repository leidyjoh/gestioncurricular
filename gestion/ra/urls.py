 # -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import RACreateView, RAUpdateView
from .views import RAListView

app_name = 'resultadoaprendizaje'

urlpatterns = [

	path('resultadoaprendizaje/crear/(<int:pk>)', RACreateView.as_view(), name='crear'),
	path('resultadoaprendizaje/listar/', RAListView.as_view(), name='listar'),
	path('resultadoaprendizaje/actualizar/<int:pk>', RAUpdateView.as_view(), name='actualizar'),
]