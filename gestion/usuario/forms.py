# -*- coding: utf-8 -*-

from django import forms
from .models import Usuario, Profesor, Estudiante
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate,login
from inicio.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core import validators
from django.core.validators import EmailValidator
from django.views.generic import TemplateView, CreateView
from django.urls import reverse, reverse_lazy



#FormView
class UserForm(LoginRequiredMixin, UserCreationForm):
	"""Permite crear un formulario con los campos del user"""

	username = forms.CharField(label='Nombre de Usuario')
	email = forms.EmailField(label='Correo Electrónico')
	first_name = forms.CharField(label='Nombres' )
	last_name = forms.CharField(label='Apellidos')
	password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
	password2 = forms.CharField(label='Confirmar Contraseña', widget=forms.PasswordInput)

	class Meta:
		"""Permite determinar el modelo User y los campos username, email, first_name,
			last_name que se muestran en el formulario Usuario"""
		model = User
		fields = ( "username", "email", "first_name", "last_name")	

#FormView
class ProfesorForm(LoginRequiredMixin, forms.ModelForm):

	class Meta:
		"""Permite determinar el modelo Usuario y los campos identificacion y tipo 
		que se muestran en el formulario"""

		model = Profesor
		fields = ['identificacion', 'imagen', 'estado', 'cargoAdministrativo', 'facultad', 'unidadAcademica', 'programa']


#FormView
class EstudianteForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite mostrar un formulario con la información de Directivo"""

	class Meta:
		"""Permite determinar el modelo Usuario y los campos identificacion y tipo 
		que se muestran en el formulario"""

		model = Estudiante
		fields = ['identificacion', 'imagen', 'estado', 'codigo', 'programa']



"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay 
errores muestra mensajes de error y si no, guarda el objeto"""	
class DocenteCreateView(LoginRequiredMixin, TemplateView):
	"""Permite mostrar el formulario para crear un usuario"""
	model = Profesor
	template_name = 'usuario/docente_form.html'
	form_class = ProfesorForm
	success_url = reverse_lazy('usuario:listar-docentes')


	def get(self,request,*args,**kwargs):
		"""Permite desplegar el formulario de creación de usuario y personal para un 
		peaje"""
		user_form = UserForm()
		docente_form = ProfesorForm()

		forms = [user_form, docente_form]
		context = {
		'section_title':'Crear Usuario Tipo Docente',
		'user_form':user_form,
		'docente_form':docente_form }

		return render(request, 'usuario/docente_form.html', context)


	def post(self,request,*args,**kwargs):
		"""Permite enviar el formulario con la información del usuario, si es valido 
		guarda los datos de lo contrario redirecciona a la lista de usuarios"""

		user_form = UserForm(request.POST)
		docente_form = ProfesorForm(request.POST,request.FILES)

		if user_form.is_valid() and docente_form.is_valid():
			new_user = user_form.save()
			new_persona = docente_form.save(commit=False)
			new_persona.user = new_user
			new_persona.save()
			messages.info(request,'Nuevo usuario tipo docente creado con exito')
			url = reverse('facultad:listar')

			return HttpResponseRedirect(url)

		user_form = UserForm(request.POST)
		docente_form = ProfesorForm(request.POST)
		messages.error(request,'Hay errores en algun campo, revise el formulario')

		context = {
		'section_title':'Crear Usuario Tipo Docente',
		'user_form':user_form,
		'docente_form': docente_form, }

		return render(request, 'usuario/docente_form.html', context)

"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay 
errores muestra mensajes de error y si no, guarda el objeto"""	
class EstudianteCreateView(LoginRequiredMixin, TemplateView):
	"""Permite mostrar el formulario para crear un usuario"""

	def get(self,request,*args,**kwargs):
		"""Permite desplegar el formulario de creación de usuario y personal para un 
		peaje"""
		user_form = UserForm()
		estudiante_form = EstudianteForm()

		forms = [user_form, estudiante_form]
		context = {
		'section_title':'Crear Usuario Tipo Estudiante',
		'user_form':user_form,
		'estudiante_form':estudiante_form }

		return render(request, 'usuario/estudiante_form.html', context)


	def post(self,request,*args,**kwargs):
		"""Permite enviar el formulario con la información del usuario, si es valido 
		guarda los datos de lo contrario redirecciona a la lista de usuarios"""

		user_form = UserForm(request.POST)
		estudiante_form = EstudianteForm(request.POST,request.FILES)

		if user_form.is_valid() and estudiante_form.is_valid():
			new_user = user_form.save()
			new_persona = estudiante_form.save(commit=False)
			new_persona.user = new_user
			new_persona.save()

			messages.info(request,'Nuevo usuario tipo estudiante creado con exito')
			url = reverse('usuario:listar-directivos')

			return HttpResponseRedirect(url)

		user_form = UserForm(request.POST)
		estudiante_form = EstudianteForm(request.POST)
		messages.error(request,'Hay errores en algun campo, revise el formulario')

		context = {
		'section_title':'Crear Usuario Tipo Estudiante',
		'user_form':user_form,
		'estudiante_form': estudiante_form, }

		return render(request, 'usuario/estudiante_form.html', context)


"""UpdateView: Una vista que muestra un formulario para editar un objeto existente, en 
caso de un error retornar el formulario, de lo contrario guarda los cambios. Esto utiliza 
un formulario generado automáticamente a partir de la clase del modelo del objeto """
class DocenteUpdateView(LoginRequiredMixin, TemplateView):
	""" Permite actualizar la información de una persona """


	def get(self, request, *args, **kwargs):
		"""Permite obtener el id y los datos almacenados en la base de datos del usuario
			que se quiere editar"""

		docente = Profesor.objects.get(id=kwargs['epk'])
		user = docente.user
		docente_form = ProfesorForm(instance=docente)
		user_form = UserForm(instance=user)

		context = {
		'section_title':'Actualizar Usuario Tipo Docente',
		'user_form':user_form,
		'docente_form':docente_form }

		return render(request, 'usuario/docente_form.html', context)


	def post(sel, request, *args, **kwargs):
		"""Permite enviar la información modificada del personal a la base de datos, 
		si es valido guarda los datos de lo contrario muestra un mensaje de error y 
		vuelve a cargar el formulario para modificar Personal"""
		
		docente = Profesor.objects.get(id=kwargs['epk'])
		user = docente.user

		docente_form = ProfesorForm(request.POST,request.FILES,instance=docente)
		user_form = UserForm(request.POST,request.FILES,instance=user)

		if user_form.is_valid() and docente_form.is_valid():

			docente.identificacion = directivo_form.cleaned_data['identificacion']
			docente.cargoAdministrativo = directivo_form.cleaned_data['cargoAdministrativo']
			docente.unidadAcademica = directivo_form.cleaned_data['unidadAcademica']
			docente.imagen = directivo_form.cleaned_data['imagen']
			docente.estado = directivo_form.cleaned_data['estado']
			docente.save()

			user.username = user_form.cleaned_data['username']
			user.first_name = user_form.cleaned_data['first_name']
			user.last_name = user_form.cleaned_data['last_name']
			user.email = user_form.cleaned_data['email']

			password1 = user_form.cleaned_data['password1']
			password2 = user_form.cleaned_data['password2']

			if password1 != "" and password2 != "":
				user.set_password(user_form.cleaned_data['password1'])
			user.save()

			""" En caso de que el administrador este modificando su misma cuenta de 
			usuario, es necesario que vuelva a ser logeado con los nuevos datos username
			y password """
			if user.id == request.user.id:
				user = authenticate(username=user.username, password=password1)
				if user is not None:
					if user.is_active:
						login(request,user)

			messages.info(request,'Usuario tipo docente modificado con exito')
			url = reverse('usuario:listar-docentes')

			return HttpResponseRedirect(url)
		else:
			docente_form = ProfesorForm(request.POST,request.FILES)
			user_form = UserForm(instance=user)

			context = {
			'section_title':'Actualizar Personal',
			'user_form':user_form,
			'docente_form':docente_form }

			return render(request, 'usuario/docente_form.html',context)

"""UpdateView: Una vista que muestra un formulario para editar un objeto existente, en 
caso de un error retornar el formulario, de lo contrario guarda los cambios. Esto utiliza 
un formulario generado automáticamente a partir de la clase del modelo del objeto """
class EstudianteUpdateView(LoginRequiredMixin, TemplateView):
	""" Permite actualizar la información de una persona """


	def get(self, request, *args, **kwargs):
		"""Permite obtener el id y los datos almacenados en la base de datos del usuario
			que se quiere editar"""

		estudiante = Estudiante.objects.get(id=kwargs['epk'])
		user = estudiante.user
		estudiante_form = EstudianteForm(instance=estudiante)
		user_form = UserForm(instance=user)

		context = {
		'section_title':'Actualizar Usuario Tipo Estudiante',
		'user_form':user_form,
		'estudiante_form':estudiante_form }

		return render(request, 'usuario/estudiante_form.html', context)


	def post(sel, request, *args, **kwargs):
		"""Permite enviar la información modificada del personal a la base de datos, 
		si es valido guarda los datos de lo contrario muestra un mensaje de error y 
		vuelve a cargar el formulario para modificar Personal"""
		
		estudiante = Estudiante.objects.get(id=kwargs['epk'])
		user = estudiante.user

		estudiante_form = EstudianteForm(request.POST,request.FILES,instance=estudiante)
		user_form = UserForm(request.POST,request.FILES,instance=user)

		if user_form.is_valid() and estudiante_form.is_valid():

			estudiante.identificacion = directivo_form.cleaned_data['identificacion']
			estudiante.codigo = directivo_form.cleaned_data['codigo']
			estudiante.programa = directivo_form.cleaned_data['programa']
			estudiante.imagen = directivo_form.cleaned_data['imagen']
			estudiante.estado = directivo_form.cleaned_data['estado']
			estudiante.save()

			user.username = user_form.cleaned_data['username']
			user.first_name = user_form.cleaned_data['first_name']
			user.last_name = user_form.cleaned_data['last_name']
			user.email = user_form.cleaned_data['email']

			password1 = user_form.cleaned_data['password1']
			password2 = user_form.cleaned_data['password2']

			if password1 != "" and password2 != "":
				user.set_password(user_form.cleaned_data['password1'])
			user.save()

			""" En caso de que el administrador este modificando su misma cuenta de 
			usuario, es necesario que vuelva a ser logeado con los nuevos datos username
			y password """
			if user.id == request.user.id:
				user = authenticate(username=user.username, password=password1)
				if user is not None:
					if user.is_active:
						login(request,user)

			messages.info(request,'Usuario tipo estudiante modificado con exito')
			url = reverse('usuario:listar-estudiantes')

			return HttpResponseRedirect(url)
		else:
			estudiante_form = EstudianteForm(request.POST,request.FILES)
			user_form = UserForm(instance=user)

			context = {
			'section_title':'Actualizar Personal',
			'user_form':user_form,
			'estudiante_form':estudiante_form }

			return render(request, 'usuario/docente_form.html',context)

