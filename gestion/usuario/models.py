# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, ResizeToFit, Adjust
from django.core.validators import EmailValidator
from facultad.models import Facultad
from unidadacademica.models import UnidadAcademica
from programa.models import Programa


tipo_estado = (
	(True, 'Activo'),
	(False, 'Inactivo'),
)

#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Usuario en base de datos"""
class Usuario(models.Model):
	"""Permite definir un listado de Usuarios"""

	#Login o user de la persona
	user = models.OneToOneField(User, default=None, null=True, on_delete=models.CASCADE, db_index=True)
	#Indentificación de la persona, debe ser única
	identificacion = models.CharField(max_length=20, unique=True, help_text="Indentificacion de la persona, debe ser unica")
	#Estado de la persona, activo/inactivo
	estado = models.BooleanField(choices=tipo_estado)
	#Imagen o foto de perfil de la persona
	imagen = models.ImageField(upload_to = "imagenes/persona", blank=True)
	#Thumbnail, que permite reducir la imagen del usuario
	thumbnail = ImageSpecField(source='imagen',
									  processors=[ResizeToFill(100, 50)],
									  format='JPEG',
									  options={'quality': 60})

	"""Permite ordenar la lista de usuarios por user y asignarle el nombre en 
	plural"""
	class Meta:
		abstract = True
		ordering = ['user']
		verbose_name_plural = "Usuarios"

	#Permite determinar una representación en string del objeto Usuario
	def __str__(self):
		return self.user.first_name + " " + self.user.last_name

	"""Permite determinar una representación en string para el objeto Usuario (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.user.first_name + " " + self.user.last_name


#Tipo de cargo adicional docente
DECANO = 'Decano'
VICEDECANO = 'Vicedecano'
ADMINISTRATIVO = 'Administrativo'
DIRECTORPROGRAMA = 'Director de Programa'
DIRECTORESCUELA = 'Director de Escuela'
NINGUNO = 'Ninguno'

tipo_cargo_adicional = (
	(DIRECTORPROGRAMA, 'Director de Programa'),
	(DIRECTORESCUELA, 'Director de Escuela'),
	(NINGUNO, 'Ninguno'),
	(DECANO, 'Decano'),
	(VICEDECANO, 'Vicedecano'),
	(ADMINISTRATIVO, 'Administrativo'),
)

"""Model: Define la estructura de la tabla Profesor que hereda de la tabla Usuario"""
class Profesor(Usuario):
	#Cargo administrativo
	cargoAdministrativo = models.CharField(max_length=50, choices=tipo_cargo_adicional, default=NINGUNO, verbose_name= ('Cargo Administrativo'))
	#Facultad a la que pertenece
	facultad = models.ForeignKey(Facultad,  on_delete=models.CASCADE)
	#Unidad academica a la que pertenece
	unidadAcademica = models.ForeignKey(UnidadAcademica, on_delete=models.CASCADE, verbose_name= ('Unidad Académica'))
	#Programa
	programa = models.ForeignKey(Programa, on_delete=models.CASCADE, null=True, blank=True)

	"""Permite ordenar la lista de profesores por unidad academica y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['cargoAdministrativo']
		verbose_name_plural = "Docentes"

	#Permite determinar una representación en string del objeto Profesro
	def __str__(self):
		return self.user.first_name +" "+ self.user.last_name

	"""Permite determinar una representación en string para el objeto Profesor (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.user.first_name +" "+ self.user.last_name


"""Model: Define la estructura de la tabla Profesor que hereda de la tabla Usuario"""
class Estudiante(Usuario):
	#Codigo del estudiante
	codigo = models.PositiveIntegerField()
	#Programas academicos a los que pertenece
	programa = models.ForeignKey(Programa, on_delete=models.CASCADE)

	"""Permite ordenar la lista de profesores por unidad academica y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['codigo']
		verbose_name_plural = "Estudiantes"

	#Permite determinar una representación en string del objeto Profesro
	def __str__(self):
		return str(self.codigo)

	"""Permite determinar una representación en string para el objeto Profesor (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.codigo)
		
