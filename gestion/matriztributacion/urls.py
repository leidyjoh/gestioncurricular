# -*- coding: utf-8 -*-
from django.urls import path
from .views import MatrizListViewSCC, MatrizListViewRA, TributacionListView, PdfRafMatrizCreateViewRA, PdfRafMatrizCreateViewSCC, TributacionCreateView, TributacionUpdateView, PdfRafMatrizCreateViewSCC2
#from .views import MatrizListView, PdfMatrizCreateView

app_name = 'matriztributacion'

urlpatterns = [

	#path('malla/crear/', MallaCreateView.as_view(), name='crear'),
	#path('matriz/listar/', MatrizListView.as_view(), name='listar'),
	#path('malla/actualizar/<int:pk>', MallaUpdateView.as_view(), name='actualizar'),
	path('matriztributacion/creartributacion/<int:pk>', TributacionCreateView.as_view(), name='crearTributacion'),
	path('matriztributacion/listartributacion/', TributacionListView.as_view(), name='listarTributacion'),
	path('matriztributacion/actualizartributacion/<int:pk>', TributacionUpdateView.as_view(), name='actualizarTributacion'),
	path('matriztributacion/listarSCC/', MatrizListViewSCC.as_view(), name='listarTributacionSCC'),
	path('matriztributacion/listarRA/', MatrizListViewRA.as_view(), name='listarTributacionRA'),
	path('matriztributacion/RAPDF/(<int:pk>)', PdfRafMatrizCreateViewRA.as_view(), name='listarRA'),
	path('matriztributacion/SCCPDF/(<int:pk>)', PdfRafMatrizCreateViewSCC.as_view(), name='listarSCC'),
	path('matriztributacion/SCCPDF2/(<int:pk>)', PdfRafMatrizCreateViewSCC2.as_view(), name='listarSCC2'),
]