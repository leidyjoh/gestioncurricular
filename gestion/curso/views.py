# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from .models import Curso, Area, AreasInternacionales
from inicio.mixins import LoginRequiredMixin
from django.shortcuts import render


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class CursoListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = Curso
	context_object_name = 'cursos'
	template_name = 'curso/curso_list.html'

	def get_queryset(self):
		#persona = self.kwargs['pk']
		#unidad = user.unidadacademica
		if self.request.user.profesor.cargoAdministrativo == "Director de Escuela":
			return Curso.objects.filter(escuela=self.request.user.profesor.unidadAcademica)
		elif self.request.user.profesor.cargoAdministrativo == "Director de Programa":
			return Curso.objects.filter(escuela=self.request.user.profesor.unidadAcademica)


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class AreaListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = Area
	context_object_name = 'areas'
	template_name = 'curso/area_list.html'

	def get_queryset(self):
		return Area.objects.filter(escuela=self.request.user.profesor.unidadAcademica)

"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class AreasInternacionalesListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = AreasInternacionales
	context_object_name = 'areasinternacionales'
	template_name = 'curso/areasinternacionales_list.html'

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		context = super(AreasInternacionalesListView,self).get_context_data(**kwargs)
		return context