from django.views.generic.edit import CreateView, UpdateView
import json
from django.db.models import Count, Sum
from django.template import Context
import random
from django.shortcuts import render
from inicio.mixins import LoginRequiredMixin
from django.views.generic import View
from datetime import date
from django import forms
from .models import MatrizTributacion
from mallacurricular.models import CursosMalla
from curso.models import Curso
from django.db.models.functions import Coalesce
from django.core.serializers.json import DjangoJSONEncoder

"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""


