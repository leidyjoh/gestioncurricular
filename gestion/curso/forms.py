# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView
from django.template import loader
from django.template import Context
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse
from django.contrib import messages
from django import forms
from inicio.mixins import LoginRequiredMixin
from .models import Curso, Area, AreasInternacionales
import json

"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""
class CursoCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""

	nombre = forms.CharField(label='Nombre')
	descripcion = forms.Textarea()
	metodologia = forms.Textarea()
	

	def clean_nombre(self):
		"""Permite validar que el campo nombre_propietario sólo contenga letras[A-Z],[a-z]"""

		n=0
		nombre = self.cleaned_data['nombre']
		if nombre.isalpha():
			return nombre
		else:
			if " " in nombre:
				separar=nombre.split(" ")
				for c in range(len(separar)):
					if separar[c].isalpha():
						n=n+1
					else:
						n=-1
						break
				if n>0:
					return nombre
				else:
					if n<0:
						raise forms.ValidationError('El nombre del curso solo puede contener letras')
			else:
				raise forms.ValidationError("El nombre del curso solo puede contener letras")	


	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = Curso
		fields = ['nombre', 'codigo', 'area', 'creditos', 'horas', 'horasClase', 'descripcion', 'tipo', 'ciclo', 'escuela', 'metodologia']

"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class CursoCreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = CursoCreateForm
	model = Curso

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para crear un VehículoEspecial"""
		context = super(CursoCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nuevo Curso'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El curso ha sido creado con exito")
		return reverse_lazy('curso:listar')

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class CursoUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = Curso
	fields = ['nombre', 'codigo', 'area', 'creditos','horas', 'horasClase', 'descripcion', 'tipo', 'ciclo', 'escuela']
	success_url = reverse_lazy('curso:listar')

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(CursoUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Curso'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El curso ha sido actualizado con exito")
		return reverse_lazy('curso:listar')

class CursoDeleteView(LoginRequiredMixin, DeleteView):
	"""Permite mientras se ejecuta contener el objeto persona sobre el que se esta 
	operando la vista"""
	
	model = Curso
	template_name = 'curso/curso_confirm_delete.html'
	success_url = reverse_lazy('curso:listar')


"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""
class AreaCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""

	nombre = forms.CharField(label='Nombre')
	

	def clean_nombre(self):
		"""Permite validar que el campo nombre_propietario sólo contenga letras[A-Z],[a-z]"""

		n=0
		nombre = self.cleaned_data['nombre']
		if nombre.isalpha():
			return nombre
		else:
			if " " in nombre:
				separar=nombre.split(" ")
				for c in range(len(separar)):
					if separar[c].isalpha():
						n=n+1
					else:
						n=-1
						break
				if n>0:
					return nombre
				else:
					if n<0:
						raise forms.ValidationError('El nombre de una área solo puede contener letras')
			else:
				raise forms.ValidationError("El nombre de una área solo puede contener letras")	


	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = Area
		fields = ['nombre', 'color', 'escuela']

"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class AreaCreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = AreaCreateForm
	model = Area

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para crear un VehículoEspecial"""
		context = super(AreaCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nuevo Área'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El área ha sido creada con exito")
		return reverse_lazy('curso:listarArea')

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class AreaUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = Area
	fields = ['nombre', 'color', 'escuela']
	success_url = reverse_lazy('curso:listarArea')

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(AreaUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Área'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El área ha sido actualizada con exito")
		return reverse_lazy('curso:listarArea')

class AreaDeleteView(LoginRequiredMixin, DeleteView):
	"""Permite mientras se ejecuta contener el objeto persona sobre el que se esta 
	operando la vista"""
	
	model = Area
	template_name = 'curso/area_confirm_delete.html'
	success_url = reverse_lazy('curso:listarArea')

class AreasInternacionalesCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""

	nombre = forms.CharField(label='Nombre')
	
	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = AreasInternacionales
		fields = ['nombre', 'color', 'escuela',]

"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class AreasInternacionalesCreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = AreasInternacionalesCreateForm
	model = AreasInternacionales

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para crear un VehículoEspecial"""
		context = super(AreasInternacionalesCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nuevo Área Internacional'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El área internacional ha sido creada con exito")
		return reverse_lazy('curso:listarAreaInternacional')

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class AreasInternacionalesUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = AreasInternacionales
	fields = ['nombre', 'color', 'escuela']
	success_url = reverse_lazy('curso:listarAreaInternacional')

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(AreasInternacionalesUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Área Internacional'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El área internacional ha sido actualizada con exito")
		return reverse_lazy('curso:listarAreaInternacional')