# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from django.urls import path
from .forms import DocenteCreateView, EstudianteCreateView, DocenteUpdateView, EstudianteUpdateView
from .views import DocenteListView, EstudianteListView

app_name = 'usuario'

urlpatterns = [

	path('usuario/docente/crear/', DocenteCreateView.as_view(), name='crear-docente'),
	path('usuario/estudiante/crear/', EstudianteCreateView.as_view(), name='crear-estudiante'),
	path('usuario/docente/listar/', DocenteListView.as_view(), name='listar-docentes'),
	path('usuario/estudiante/listar/', EstudianteListView.as_view(), name='listar-estudiantes'),
	path('usuario/docente/actualizar/<int:epk>', DocenteUpdateView.as_view(), name='actualizar-docente'),
	path('usuario/estudiante/actualizar/<int:epk>', EstudianteUpdateView.as_view(), name='actualizar-estudiante'),
]