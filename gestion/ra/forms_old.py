# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import TemplateView
from django.template import loader
from django.template import Context
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse
from django.contrib import messages
from django import forms
from inicio.mixins import LoginRequiredMixin
from programa.models import Programa
from .models import ResultadoAprendizaje
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget
import json
"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""
class ResultadoAprendizajeCreateForm(LoginRequiredMixin, forms.ModelForm):

	descripcion = forms.CharField(widget=SummernoteWidget(), label='Descripción')

	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = ResultadoAprendizaje
		widgets = {'descripcion': SummernoteWidget()}
		fields = ['id', 'numero','accion', 'contenido', 'contexto', 'finalidad', 'descripcion', 'nivelAprendizaje', 'programa', 'curso', 'porcentaje']



	
"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class RACreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = ResultadoAprendizajeCreateForm
	model = ResultadoAprendizaje

	def get_context_data(self,**kwargs):
		context = super(RACreateView, self).get_context_data(**kwargs)
		context['section_title'] = 'Nuevo Resultado de Aprendizaje'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"el resultado de aprendizaje ha sido creado con exito")
		return reverse_lazy('ra:listar')

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class RAUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = ResultadoAprendizaje
	fields = ['id', 'numero','accion', 'contenido', 'contexto', 'finalidad', 'descripcion', 'nivelAprendizaje', 'programa', 'curso']
	success_url = reverse_lazy('ra:listar')

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(RAUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Resultado de Aprendizaje'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El resultado de aprendizaje ha sido actualizado con exito")
		return reverse_lazy('ra:listar')
