# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from .models import ResultadoAprendizaje
from inicio.mixins import LoginRequiredMixin
from django.shortcuts import render


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class RAListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = ResultadoAprendizaje
	context_object_name = 'ras'
	template_name = 'ra/resultadoaprendizaje_list.html'

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		context = super(RAListView,self).get_context_data(**kwargs)
		return context