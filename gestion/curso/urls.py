 # -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import CursoCreateView, CursoUpdateView, CursoDeleteView, AreaCreateView, AreaUpdateView, AreasInternacionalesCreateView, AreasInternacionalesUpdateView, AreaDeleteView
from .views import CursoListView, AreaListView, AreasInternacionalesListView

app_name = 'curso'

urlpatterns = [

	path('curso/crear/', CursoCreateView.as_view(), name='crear'),
	path('curso/listar/', CursoListView.as_view(), name='listar'),
	path('curso/actualizar/<int:pk>', CursoUpdateView.as_view(), name='actualizar'),
	path('curso/eliminar/<int:pk>', CursoDeleteView.as_view(), name='eliminar'),
	path('curso/creararea/', AreaCreateView.as_view(), name='crearArea'),
	path('curso/listararea/', AreaListView.as_view(), name='listarArea'),
	path('curso/actualizararea/<int:pk>', AreaUpdateView.as_view(), name='actualizarArea'),
	path('curso/eliminararea/<int:pk>', AreaDeleteView.as_view(), name='eliminarArea'),
	path('curso/crearareainternacional/', AreasInternacionalesCreateView.as_view(), name='crearAreaInternacional'),
	path('curso/listarareainternacional/', AreasInternacionalesListView.as_view(), name='listarAreaInternacional'),
	path('curso/actualizarareainternacional/<int:pk>', AreasInternacionalesUpdateView.as_view(), name='actualizarAreaInternacional'),
]