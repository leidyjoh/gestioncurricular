# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import ListView, TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from .models import MatrizTributacion, CursosTributacionMatriz
from django import forms
from mallacurricular.models import CursosMalla, MallaCurricular
from programa.models import Programa
from perfil.models import Perfil, Competencias
from ra.models import ResultadoAprendizaje
from curso.models import Curso, Area
from scc.models import SCC, SCCEspecificas
from sccfacultad.models import SCCFacultad
from semestre.models import Semestre
from inicio.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.core.serializers.json import DjangoJSONEncoder
from django.forms.models import modelformset_factory
import json
from django.contrib import messages
from django.forms import formset_factory, inlineformset_factory
from itertools import chain, islice


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class MatrizListViewSCC(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = MatrizTributacion
	context_object_name = 'matrices'
	template_name = 'matriztributacion/matriztributacion_list_SCC.html'


	def get_queryset(self):
		#persona = self.kwargs['pk']
		#unidad = user.unidadacademica
		if self.request.user.profesor.cargoAdministrativo == "Director de Escuela":
			return MatrizTributacion.objects.filter(programa__unidadacademica=self.request.user.profesor.unidadAcademica)
		elif self.request.user.profesor.cargoAdministrativo == "Director de Programa":
			return MatrizTributacion.objects.filter(programa=self.request.user.profesor.programa)


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class MatrizListViewRA(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = MatrizTributacion
	context_object_name = 'matrices'
	template_name = 'matriztributacion/matriztributacion_list_RA.html'

	def get_queryset(self):
		#persona = self.kwargs['pk']
		#unidad = user.unidadacademica
		if self.request.user.profesor.cargoAdministrativo == "Director de Escuela":
			return MatrizTributacion.objects.filter(programa__unidadacademica=self.request.user.profesor.unidadAcademica)
		elif self.request.user.profesor.cargoAdministrativo == "Director de Programa":
			return MatrizTributacion.objects.filter(programa=self.request.user.profesor.programa)


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class TributacionListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = Programa
	context_object_name = 'tributaciones'
	template_name = 'matriztributacion/tributacion_list.html'
	

	def get_queryset(self):
		#persona = self.kwargs['pk']
		#unidad = user.unidadacademica
		if self.request.user.profesor.cargoAdministrativo == "Director de Escuela":
			return Programa.objects.filter(unidadacademica=self.request.user.profesor.unidadAcademica)
		elif self.request.user.profesor.cargoAdministrativo == "Director de Programa":
			return Programa.objects.filter(unidadacademica__programa=self.request.user.profesor.programa)

class TributacionCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""
	
	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = MatrizTributacion
		fields = ['programa']

class MatrizTributacionCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""
	
	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = CursosTributacionMatriz
		fields = ['matriz', 'curso', 'scc']

	#def __init__(self, *args, **kwargs):
		#programa_id = kwargs.pop('programa_id')
		#super(MatrizTributacionCreateForm, self).__init__(*args, **kwargs)
		#self.request = kwargs.pop('request')
		# Override the product query set with a list of product excluding those already in the pricelist
		#programa = MatrizTributacion.objects.get(id=kwargs['pk'])
		#print("programa")
		#print(programa)
		
		#programa = Programa.objects.get(id=programa_id)
		#cursos = CursosMalla.objects.filter(malla__programa=programa)
		#perfil=Perfil.objects.get(programa=programa)
		#sccs=perfil.sccFacultad.all()
		#scc = """
		#scc = Perfil.objects.filter(perfil=9).values_list("sccfacultad")
		
		#sccgenericas = SCC.objects.filter(perfil=9).values_list("titulo")
		#sccespecificas = SCCEspecificas.objects.filter(perfil=9).values_list('titulo')
		
		#union1 = sccgenericas = SCC.objects.filter(perfil=9).values_list("titulo")
		#union2 = sccgenericas.union(sccespecificas)
		#print(union2)
		
		#self.fields['scc'].queryset = Competencias
		#self.fields['curso'].queryset = CursosMalla.objects.filter(malla__programa=programa_id)

TributacionFormSet = inlineformset_factory(MatrizTributacion, CursosTributacionMatriz, form=MatrizTributacionCreateForm, can_delete=False, extra=20)

class TributacionCreateView(LoginRequiredMixin, CreateView):
	#model = MatrizTributacion
	#template_name = 'matriztributacion/perfil_form.html'
	#form_class = PerfilCreateForm
	#success_url = reverse_lazy('perfil:listar')

	def get_form_kwargs(self):
		kwargs = super(TributacionCreateView, self).get_form_kwargs()
		kwargs['programa_id'] = self.request.user.profesor.programa
		return kwargs


	def get(self, request, *args, **kwargs):
		"""Primero ponemos nuestro object como nulo, se debe tener en
		cuenta que object se usa en la clase CreateView para crear el objeto"""
		self.object = None
		form = MatrizTributacionCreateForm()
		formset = TributacionFormSet()
		programa_id = self.kwargs['pk']
		print(programa_id)
		programa = Programa.objects.get(id=programa_id)
		sccgenericas = SCC.objects.filter(perfil__programa=programa).values_list("titulo")
		sccespecificas = SCCEspecificas.objects.filter(perfil__programa=programa).values_list('titulo')
		#cursos = CursosMalla.objects.filter(malla__programa=programa)
		form.fields['scc'].queryset = sccgenericas.union(sccespecificas)
		forms = [form, formset]
		context = {
		'section_title':'Nueva tributacion',
		'form':form,
		'formset':formset}
		return render(request, 'matriztributacion/matriztributacion_form.html', context)



	def post(self, request, *args, **kwargs):

	    form = MatrizTributacionCreateForm(request.POST)
	    formset = TributacionFormSet(request.POST)


	    if form.is_valid() and formset.is_valid():
	    	return self.form_valid(form, formset)
	    else:
	    	return self.form_invalid(form, formset)


	def form_valid(self, form, formset):
		self.object = form.save()
		formset.instance = self.object
		formset.save()
		url = reverse('matriztributacion:listarTributacion')
		messages.info(self.request,'Nueva tributación creada con exito')
		return HttpResponseRedirect(url)


	def form_invalid(self, form, formset):
		context = {
		'section_title':'Nueva Tributacion',
		'form':form,
		'formset':formset }
		messages.info(self.request,'Hay errores en algun campo, revise el formulario')
		return render(self.request, 'matriztributacion/matriztributacion_form.html', context)

"""def TributacionCreateView(request):
	form = TributacionCreateForm()
	formset = TributacionFormSet()

	if request.method=='POST':
		form = TributacionCreateForm(request.POST)
		formset = TributacionFormSet(request.POST, request.FILES)

		if form.is_valid() and formset.is_valid():
			for form_ambito in formset:
				obj_ambito = form_ambito.save(commit=False)
				obj_ambito.programa = form.cleaned_data["programa"]
				obj_ambito.save()

			context = {
			'section_title':'Nuevo Perfil',
			'form':form,
			'formset':formset}
			url = reverse('matriztributacion:listarTributacion')
			messages.info(request,'El perfil de egresado ha sido creado con éxito')
			return HttpResponseRedirect(url)

		else:
			context = {
			'section_title':'Nuevo Perfil',
			#'form':form,
			'formset':formset}
			messages.info(request,'Hay errores en algun campo, revise el formulario')
			return render(request, 'perfil/perfil_form.html', context)

	return render(request, 'matriztributacion/matriztributacion_form.html', {'form':form, 'formset': formset,})
"""
"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class TributacionUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = MatrizTributacion
	fields = ['curso', 'scc']
	success_url = reverse_lazy('curso:listarTributacion')

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(TributacionUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Tributacion'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"La tributación ha sido actualizada con exito")
		return reverse_lazy('curso:listarTributacion')
		
class MatrizRACreateForm(forms.ModelForm):

	#programa=forms.queryset=Sheker.objects.all()
	
	class Meta:
		"""Permite determinar el modelo HorarioCarril, con los campos tipo, peaje, carril, 
		fecha, horai y horaf que se muestran, como filtros en el formulario"""
		model = MatrizTributacion
		fields = ['programa']

class MatrizSCCCreateForm(forms.ModelForm):

	#programa=forms.queryset=Sheker.objects.all()
	
	class Meta:
		"""Permite determinar el modelo HorarioCarril, con los campos tipo, peaje, carril, 
		fecha, horai y horaf que se muestran, como filtros en el formulario"""
		model = MatrizTributacion
		fields = ['programa']
"""View:"""
class PdfRafMatrizCreateViewRA(LoginRequiredMixin, TemplateView):

	def get(self,request,*args,**kwargs):

		form = MatrizRACreateForm()
		matriz = MatrizTributacion.objects.get(id=kwargs['pk'])
		programa = matriz.programa
		malla = MallaCurricular.objects.get(programa=programa.id)
		print("PROGRAMA")
		print(programa)
		print("MALLA")
		print(malla)
		cursos = CursosMalla.objects.filter(malla=malla.id)
		ras = ResultadoAprendizaje.objects.filter(programa=programa.id)
		
		resultadosa = ras.values("id", "descripcion", "accion")
		cursosmalla = cursos.values("curso", "curso__nombre", "curso__area__color").order_by("curso__nombre")
		cursosra = cursos.values("curso__id", "resultadoaprendizaje", "resultadoaprendizaje__accion", "resultadoaprendizaje__descripcion", "curso__nombre", "curso__area__color").order_by("curso__nombre")
		
		for m in cursosmalla:
			m['nombre'] = str(Curso.objects.get(id=m["curso"]).nombre)
		
		cursos2 = json.dumps(list(cursosmalla), cls=DjangoJSONEncoder)
		cursosRa = json.dumps(list(cursosra), cls=DjangoJSONEncoder)
		ra = json.dumps(list(resultadosa), cls=DjangoJSONEncoder)
		"""context = {'form':form,
		'cursos2':cursos2}"""
		print("CURSOS")
		print(cursos2)
		print(cursosRa)
		print(ra)
		context = {'form':form,
		'cursos2':cursos2,
		'cursosRa':cursosRa,
		'ra':ra}
		

		return render(request, 'matriztributacion/matrizRA.html', context)



		"""programa = request.POST.get('programa')
		print(programa)

		form = MatrizRACreateForm(request.POST)
		cursos = CursosMalla.objects.all()
		print(cursos)
		cursosmalla = cursos.values("curso")

		for m in cursosmalla:
			m['cursos'] = str(Curso.objects.get(id=m["curso"]).nombre)
		
		cursos2 = json.dumps(list(cursosmalla), cls=DjangoJSONEncoder)
		print("AQUI")
		print(cursosmalla)
		context = {
		'form':form,
		'cursosmalla':cursosmalla, 
		'cursos2':cursos2, 
		}"""

		return render(request, 'matriztributacion/matrizRA.html', context)

class PdfRafMatrizCreateViewSCC(LoginRequiredMixin, TemplateView):

	def get(self,request,*args,**kwargs):

		form = MatrizSCCCreateForm()
		matriz = MatrizTributacion.objects.get(id=kwargs['pk'])
		programa = matriz.programa
		malla = MallaCurricular.objects.get(programa=programa.id)
		perfil = Perfil.objects.get(programa=programa)
		print("PROGRAMA")
		print(programa)
		print("MALLA")
		print(malla)
		escuela = programa.unidadacademica
		areas = Area.objects.filter(escuela=escuela)
		areasSN=areas.exclude(nombre='Ninguna')
		areas2 = areasSN.values("nombre", "color")
		cursos = CursosMalla.objects.filter(malla=malla.id)
		sccs = Competencias.objects.filter(perfil=perfil)
		cursoxscc = CursosTributacionMatriz.objects.filter(matriz=matriz)
		print("CURSOSXSCC")
		print(cursoxscc)
		cursosmalla = cursos.values("curso", "curso__nombre", "curso__area__color", "semestre").order_by("curso__nombre")
		print("CURSOSXSCC")
		print(cursosmalla)
		sccnombre = sccs.values("id", "titulo" ).order_by("titulo")
		cursosscc = cursoxscc.values("curso__id", "scc", "curso__curso__nombre", "scc__titulo", "curso__curso__area__color").order_by("curso__curso__nombre")

		#for m in cursosmalla:
			#m['nombre'] = str(Curso.objects.get(id=m["curso"]).nombre)

		scc2 = json.dumps(list(sccnombre), cls=DjangoJSONEncoder)
		cursos2 = json.dumps(list(cursosmalla), cls=DjangoJSONEncoder)
		cursosSCC = json.dumps(list(cursosscc), cls=DjangoJSONEncoder)
		areaEscuela = json.dumps(list(areas2), cls=DjangoJSONEncoder)

		print("CURSOSSCC")
		print(cursosSCC)
		"""context = {'form':form,
		'cursos2':cursos2}"""
		context = {'form':form,
		'cursos2':cursos2,
		'cursosSCC':cursosSCC,
		'scc2': scc2,
		'areaEscuela': areaEscuela,}

		return render(request, 'matriztributacion/matrizSCC.html', context)



		"""programa = request.POST.get('programa')
		print(programa)

		form = MatrizRACreateForm(request.POST)
		cursos = CursosMalla.objects.all()
		print(cursos)
		cursosmalla = cursos.values("curso")

		for m in cursosmalla:
			m['cursos'] = str(Curso.objects.get(id=m["curso"]).nombre)
		
		cursos2 = json.dumps(list(cursosmalla), cls=DjangoJSONEncoder)
		print("AQUI")
		print(cursosmalla)
		context = {
		'form':form,
		'cursosmalla':cursosmalla, 
		'cursos2':cursos2, 
		}"""

		return render(request, 'matriztributacion/matrizRA.html', context)

class PdfRafMatrizCreateViewSCC2(LoginRequiredMixin, TemplateView):

	def get(self,request,*args,**kwargs):

		form = MatrizSCCCreateForm()
		matriz = MatrizTributacion.objects.get(id=kwargs['pk'])
		programa = matriz.programa
		malla = MallaCurricular.objects.get(programa=programa.id)
		perfil = Perfil.objects.get(programa=programa)
		print("PROGRAMA")
		print(programa)
		print("MALLA")
		print(malla)
		escuela = programa.unidadacademica
		areas = Area.objects.filter(escuela=escuela)
		areasSN = areas.exclude(nombre='Ninguna')
		areas2 = areasSN.values("nombre", "color")
		cursos = CursosMalla.objects.filter(malla=malla.id)
		sccs = Competencias.objects.filter(perfil=perfil)
		cursoxscc = CursosTributacionMatriz.objects.filter(matriz=matriz)
		print("CURSOSXSCC")
		print(cursoxscc)
		cursosmalla = cursos.values("curso", "curso__nombre", "curso__area__color", "semestre").order_by("curso__nombre")
		print("CURSOSXSCC")
		print(cursosmalla)
		sccnombre = sccs.values("id", "titulo" ).order_by("titulo")
		cursosscc = cursoxscc.values("curso__id", "scc", "curso__curso__nombre", "scc__titulo", "curso__curso__area__color").order_by("curso__curso__nombre")

		#for m in cursosmalla:
			#m['nombre'] = str(Curso.objects.get(id=m["curso"]).nombre)

		scc2 = json.dumps(list(sccnombre), cls=DjangoJSONEncoder)
		cursos2 = json.dumps(list(cursosmalla), cls=DjangoJSONEncoder)
		cursosSCC = json.dumps(list(cursosscc), cls=DjangoJSONEncoder)
		areaEscuela = json.dumps(list(areas2), cls=DjangoJSONEncoder)

		print("CURSOSSCC")
		print(cursosSCC)
		"""context = {'form':form,
		'cursos2':cursos2}"""
		context = {'form':form,
		'cursos2':cursos2,
		'cursosSCC':cursosSCC,
		'scc2': scc2,
		'areaEscuela': areaEscuela,}

		return render(request, 'matriztributacion/matrizSCC2.html', context)



		"""programa = request.POST.get('programa')
		print(programa)

		form = MatrizRACreateForm(request.POST)
		cursos = CursosMalla.objects.all()
		print(cursos)
		cursosmalla = cursos.values("curso")

		for m in cursosmalla:
			m['cursos'] = str(Curso.objects.get(id=m["curso"]).nombre)
		
		cursos2 = json.dumps(list(cursosmalla), cls=DjangoJSONEncoder)
		print("AQUI")
		print(cursosmalla)
		context = {
		'form':form,
		'cursosmalla':cursosmalla, 
		'cursos2':cursos2, 
		}"""

		return render(request, 'matriztributacion/matrizRA.html', context)

		
