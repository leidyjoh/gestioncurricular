# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from .models import Facultad, CicloAsignatura, TipoAsignatura, FormacionGeneral, NivelAprendizaje
from inicio.mixins import LoginRequiredMixin
from django.shortcuts import render


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class FacultadListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = Facultad
	context_object_name = 'facultades'
	template_name = 'facultad/facultad_list.html'

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		context = super(FacultadListView,self).get_context_data(**kwargs)
		return context


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class CicloListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = CicloAsignatura
	context_object_name = 'ciclos'
	template_name = 'facultad/ciclo_list.html'

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		context = super(CicloListView,self).get_context_data(**kwargs)
		return context


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class TipoAsignaturaListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = TipoAsignatura
	context_object_name = 'asignaturas'
	template_name = 'facultad/tipoasignatura_list.html'

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		context = super(TipoAsignaturaListView,self).get_context_data(**kwargs)
		return context

"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class FormacionListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = FormacionGeneral
	context_object_name = 'formaciones'
	template_name = 'facultad/formacion_list.html'

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		context = super(FormacionListView,self).get_context_data(**kwargs)
		return context

"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class NivelAprendizajeListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = NivelAprendizaje
	context_object_name = 'niveles'
	template_name = 'facultad/nivelaprendizaje_list.html'

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		context = super(NivelAprendizajeListView,self).get_context_data(**kwargs)
		return context