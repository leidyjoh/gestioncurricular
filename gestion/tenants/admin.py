# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Tenant


"""Permite administrar la visualización de los datos de Facultad en la base de datos del
	sitio de administración"""
class AdminTenant(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Facultad que se mostrarán en el sitio de administración"""
	list_display = ('id', 'nombre')
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Facultad en el sitio 
	de administración"""
	search_fields = ('nombre',)

#Permite registrar las clases Facultad y AdminFacultad
admin.site.register(Tenant, AdminTenant)