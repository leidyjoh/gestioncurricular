# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Facultad, TipoAsignatura, CicloAsignatura, FormacionGeneral, NivelAprendizaje


"""Permite administrar la visualización de los datos de Facultad en la base de datos del
	sitio de administración"""
class AdminFacultad(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Facultad que se mostrarán en el sitio de administración"""
	list_display = ('id', 'nombre')
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Facultad en el sitio 
	de administración"""
	search_fields = ('nombre',)

#Permite registrar las clases Facultad y AdminFacultad
admin.site.register(Facultad, AdminFacultad)


"""Permite administrar la visualización de los datos de Facultad en la base de datos del
	sitio de administración"""
class AdminCicloAsignatura(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Facultad que se mostrarán en el sitio de administración"""
	list_display = ('id', 'nombre', 'color', 'facultad')
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Facultad en el sitio 
	de administración"""
	search_fields = ('nombre',)

#Permite registrar las clases Facultad y AdminFacultad
admin.site.register(CicloAsignatura, AdminCicloAsignatura)

"""Permite administrar la visualización de los datos de Facultad en la base de datos del
	sitio de administración"""
class AdminTipoAsignatura(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Facultad que se mostrarán en el sitio de administración"""
	list_display = ('id', 'nombre', 'abreviatura', 'facultad')
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Facultad en el sitio 
	de administración"""
	search_fields = ('abreviatura',)


#Permite registrar las clases Facultad y AdminFacultad
admin.site.register(TipoAsignatura, AdminTipoAsignatura)
"""Permite administrar la visualización de los datos de Facultad en la base de datos del
	sitio de administración"""
class AdminFormacionGeneral(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Facultad que se mostrarán en el sitio de administración"""
	list_display = ('id', 'nombre', 'abreviatura', 'facultad', 'color')
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Facultad en el sitio 
	de administración"""
	search_fields = ('abreviatura',)

#Permite registrar las clases Facultad y AdminFacultad
admin.site.register(FormacionGeneral, AdminFormacionGeneral)


class AdminNivelAprendizaje(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Facultad que se mostrarán en el sitio de administración"""
	list_display = ('id', 'nombre', 'facultad',)
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Facultad en el sitio 
	de administración"""
	search_fields = ('nombre',)

#Permite registrar las clases Facultad y AdminFacultad
admin.site.register(NivelAprendizaje, AdminNivelAprendizaje)

