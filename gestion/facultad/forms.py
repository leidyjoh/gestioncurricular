# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView
from django.template import loader
from django.template import Context
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse
from django.contrib import messages
from django import forms
from inicio.mixins import LoginRequiredMixin
from .models import Facultad, CicloAsignatura, TipoAsignatura, FormacionGeneral, NivelAprendizaje
import json

"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""
class FacultadCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""

	nombre = forms.CharField(label='Nombre')
	

	def clean_nombre(self):
		"""Permite validar que el campo nombre_propietario sólo contenga letras[A-Z],[a-z]"""

		n=0
		nombre = self.cleaned_data['nombre']
		if nombre.isalpha():
			return nombre
		else:
			if " " in nombre:
				separar=nombre.split(" ")
				for c in range(len(separar)):
					if separar[c].isalpha():
						n=n+1
					else:
						n=-1
						break
				if n>0:
					return nombre
				else:
					if n<0:
						raise forms.ValidationError('El nombre de la facultad solo puede contener letras')
			else:
				raise forms.ValidationError("El nombre de la facultad solo puede contener letras")	


	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = Facultad
		fields = ['nombre']

"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class FacultadCreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = FacultadCreateForm
	model = Facultad


	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para crear un VehículoEspecial"""
		context = super(FacultadCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nueva Facultad'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"La facultad ha sido creada con exito")
		return reverse_lazy('facultad:listar')

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class FacultadUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = Facultad
	fields = ['nombre']
	success_url = reverse_lazy('facultad:listar')
	

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(FacultadUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Facultad'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"La facultad ha sido actualizada con exito")
		return reverse_lazy('facultad:listar')

"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""
class CicloCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""

	nombre = forms.CharField(label='Nombre')
	

	def clean_nombre(self):
		"""Permite validar que el campo nombre_propietario sólo contenga letras[A-Z],[a-z]"""

		n=0
		nombre = self.cleaned_data['nombre']
		if nombre.isalpha():
			return nombre
		else:
			if " " in nombre:
				separar=nombre.split(" ")
				for c in range(len(separar)):
					if separar[c].isalpha():
						n=n+1
					else:
						n=-1
						break
				if n>0:
					return nombre
				else:
					if n<0:
						raise forms.ValidationError('El nombre del ciclo solo puede contener letras')
			else:
				raise forms.ValidationError("El nombre del ciclo solo puede contener letras")	


	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = CicloAsignatura
		fields = ['nombre', 'color', 'facultad']

"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class CicloCreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = CicloCreateForm
	model = CicloAsignatura

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para crear un VehículoEspecial"""
		context = super(CicloCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nuevo Ciclo'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El ciclo ha sido creado con exito")
		return reverse_lazy('facultad:listarciclo')

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class CicloUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = CicloAsignatura
	fields = ['nombre', 'color', 'facultad']
	success_url = reverse_lazy('facultad:listarciclo')

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(CicloUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Ciclo'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"Ciclo ha sido actualizada con exito")
		return reverse_lazy('facultad:listarciclo')


class CicloDeleteView(LoginRequiredMixin, DeleteView):
	"""Permite mientras se ejecuta contener el objeto persona sobre el que se esta 
	operando la vista"""
	
	model = CicloAsignatura
	template_name = 'facultad/ciclo_confirm_delete.html'
	success_url = reverse_lazy('facultad:listarciclo')
"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""
class TipoAsignaturaCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""

	nombre = forms.CharField(label='Nombre')
	

	def clean_nombre(self):
		"""Permite validar que el campo nombre_propietario sólo contenga letras[A-Z],[a-z]"""

		n=0
		nombre = self.cleaned_data['nombre']
		if nombre.isalpha():
			return nombre
		else:
			if " " in nombre:
				separar=nombre.split(" ")
				for c in range(len(separar)):
					if separar[c].isalpha():
						n=n+1
					else:
						n=-1
						break
				if n>0:
					return nombre
				else:
					if n<0:
						raise forms.ValidationError('El nombre del tipo de asignatura solo puede contener letras')
			else:
				raise forms.ValidationError("El nombre del tipo de asignatura solo puede contener letras")	


	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = TipoAsignatura
		fields = ['nombre', 'abreviatura', 'facultad']


class FormacionCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""

	nombre = forms.CharField(label='Nombre')
	

	def clean_nombre(self):
		"""Permite validar que el campo nombre_propietario sólo contenga letras[A-Z],[a-z]"""

		n=0
		nombre = self.cleaned_data['nombre']
		if nombre.isalpha():
			return nombre
		else:
			if " " in nombre:
				separar=nombre.split(" ")
				for c in range(len(separar)):
					if separar[c].isalpha():
						n=n+1
					else:
						n=-1
						break
				if n>0:
					return nombre
				else:
					if n<0:
						raise forms.ValidationError('El nombre de la formación general solo puede contener letras')
			else:
				raise forms.ValidationError("El nombre de la formación general solo puede contener letras")	


	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = FormacionGeneral
		fields = ['nombre', 'abreviatura', 'color', 'facultad',]


class NivelAprendizajeCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""

	nombre = forms.CharField(label='Nombre')

	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = NivelAprendizaje
		fields = ['nombre', 'facultad',]


"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class TipoAsignaturaCreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = TipoAsignaturaCreateForm
	model = TipoAsignatura

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para crear un VehículoEspecial"""
		context = super(TipoAsignaturaCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nuevo Tipo de Asignatura'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El tipo de asignatura ha sido creado con exito")
		return reverse_lazy('facultad:listartipoasignatura')

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class TipoAsignaturaUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = TipoAsignatura
	fields = ['nombre', 'abreviatura', 'facultad']
	success_url = reverse_lazy('facultad:listartipoasignatura')

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(TipoAsignaturaUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Tipo de Asignatura'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El tipo de asignatura ha sido actualizado con exito")
		return reverse_lazy('facultad:listartipoasignatura')

class TipoAsignaturaDeleteView(LoginRequiredMixin, DeleteView):
	"""Permite mientras se ejecuta contener el objeto persona sobre el que se esta 
	operando la vista"""
	
	model = TipoAsignatura
	template_name = 'facultad/tipoasignatura_confirm_delete.html'
	success_url = reverse_lazy('facultad:listartipoasignatura')

"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class FormacionCreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = FormacionCreateForm
	model = FormacionGeneral

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para crear un VehículoEspecial"""
		context = super(FormacionCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nueva Formación General'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"La formación general ha sido creada con exito")
		return reverse_lazy('facultad:listarformacion')

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class FormacionUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = FormacionGeneral
	fields = ['nombre', 'abreviatura', 'facultad', 'color']
	success_url = reverse_lazy('facultad:listarformacion')

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(FormacionUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Formación General'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"La formación general ha sido actualizada con exito")
		return reverse_lazy('facultad:listartipoasignatura')

class FormacionDeleteView(LoginRequiredMixin, DeleteView):
	"""Permite mientras se ejecuta contener el objeto persona sobre el que se esta 
	operando la vista"""
	
	model = FormacionGeneral
	template_name = 'facultad/formacion_confirm_delete.html'
	success_url = reverse_lazy('facultad:listarformacion')



"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class NivelAprendizajeCreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = NivelAprendizajeCreateForm
	model = NivelAprendizaje

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para crear un VehículoEspecial"""
		context = super(NivelAprendizajeCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nuevo Nivel de Aprendizaje'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El nivel de aprendizaje ha sido creado con exito")
		return reverse_lazy('facultad:listarnivelaprendizaje')

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class NivelAprendizajeUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = NivelAprendizaje
	fields = ['nombre', 'facultad']
	success_url = reverse_lazy('facultad:listarnivelaprendizaje')

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(NivelAprendizajeUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Nivel de Aprendizaje'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El nivel de aprendizaje ha sido actualizado con exito")
		return reverse_lazy('facultad:listarnivelaprendizaje')

class NivelAprendizajeDeleteView(LoginRequiredMixin, DeleteView):
	"""Permite mientras se ejecuta contener el objeto persona sobre el que se esta 
	operando la vista"""
	
	model = NivelAprendizaje
	template_name = 'facultad/nivelaprendizaje_confirm_delete.html'
	success_url = reverse_lazy('facultad:listarniveldeaprendizaje')
