# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from ra.models import ResultadoAprendizaje
from curso.models import Curso
from smart_selects.db_fields import ChainedForeignKey
from smart_selects.db_fields import GroupedForeignKey
from django.db.models.query import QuerySet
from django_group_by import GroupByMixin
# Create your models here.
#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class Evaluacion(models.Model):
	#Curso
	curso = models.ForeignKey(Curso, on_delete=models.CASCADE, related_name="CursosRa")
	#RA
	ra = models.ForeignKey(ResultadoAprendizaje, on_delete=models.CASCADE, verbose_name= ('RA'))
	#Porcentaje
	porcentaje = models.FloatField()
	
	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['curso']
		verbose_name_plural = "Evaluacion Cursos"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.curso.nombre

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.curso.nombre