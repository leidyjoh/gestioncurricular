from django.conf.urls import include, url
#from .views import Editar
#from .views import EditarContrasenia
from .views import Perfil

app_name = 'cuenta'

"""Define las direcciones web o url de la aplicación Cuenta"""
urlpatterns = [
	#url(r'^cuenta/editar$', Editar.as_view(), name='editar'),
	#url(r'^cuenta/editar_contrasenia$', EditarContrasenia.as_view(), name='editar_contrasenia'),
	url(r'^cuenta/perfil$', Perfil.as_view(), name='perfil'),
]