 # -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import SCCFacultadCreateView, SCCFacultadUpdateView, SCCFacultadDeleteView
from .views import SCCFacultadListView

app_name = 'sccfacultad'

urlpatterns = [

	path('sccfacultad/crear/', SCCFacultadCreateView.as_view(), name='crear'),
	path('sccfacultad/listar/', SCCFacultadListView.as_view(), name='listar'),
	path('sccfacultad/actualizar/<int:pk>', SCCFacultadUpdateView.as_view(), name='actualizar'),
	path('sccfacultad/eliminar/<int:pk>', SCCFacultadDeleteView.as_view(), name='eliminar'),

]