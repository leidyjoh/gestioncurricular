# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Curso, Area, AreasInternacionales


"""Permite administrar la visualización de los datos de Facultad en la base de datos del
	sitio de administración"""
class AdminCurso(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Facultad que se mostrarán en el sitio de administración"""
	list_display = ('id', 'nombre', 'codigo', 'creditos', 'horas', 'horasClase', 'descripcion', 'tipo', 'ciclo', 'escuela')
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Facultad en el sitio 
	de administración"""
	search_fields = ('nombre', 'codigo')

#Permite registrar las clases Facultad y AdminFacultad
admin.site.register(Curso, AdminCurso)

"""Permite administrar la visualización de los datos de Facultad en la base de datos del
	sitio de administración"""
class AdminArea(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Facultad que se mostrarán en el sitio de administración"""
	list_display = ('id', 'nombre', 'escuela', 'color')
	"""Permite establecer el parametro de busqueda nombre de la tabla Facultad en el sitio 
	de administración"""
	search_fields = ('nombre', 'escuela')

#Permite registrar las clases Facultad y AdminFacultad
admin.site.register(Area, AdminArea)

class AdminAreasInternacionales(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Facultad que se mostrarán en el sitio de administración"""
	list_display = ('id', 'nombre', 'escuela', 'color')
	"""Permite establecer el parametro de busqueda nombre de la tabla Facultad en el sitio 
	de administración"""
	search_fields = ('nombre', 'escuela')

#Permite registrar las clases Facultad y AdminFacultad
admin.site.register(AreasInternacionales, AdminAreasInternacionales)


