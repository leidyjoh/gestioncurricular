# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from facultad.models import CicloAsignatura, TipoAsignatura
from unidadacademica.models import UnidadAcademica
from colorfield.fields import ColorField


#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class Area(models.Model):
	"""Permite definir un listado de Cursos"""

	#Nombre del curso
	nombre = models.CharField(max_length=200)
	#escuela que lo gestiona
	color = ColorField(default='#FF0000', verbose_name= ('Color '))
	#escuela que lo gestiona
	escuela = models.ForeignKey(UnidadAcademica, on_delete=models.CASCADE)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Areas"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.nombre

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.nombre 


#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class AreasInternacionales(models.Model):
	"""Permite definir un listado de Cursos"""

	#Nombre del curso
	nombre = models.CharField(max_length=200)
	#escuela que lo gestiona
	color = ColorField(default='#FF0000', verbose_name= ('Color '))
	#escuela que lo gestiona
	escuela = models.ForeignKey(UnidadAcademica, on_delete=models.CASCADE)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Areas Internacionales"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.nombre

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.nombre



#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class Curso(models.Model):
	"""Permite definir un listado de Cursos"""

	#Nombre del curso
	nombre = models.CharField(max_length=200)
	#Código del curso
	codigo = models.CharField(max_length=200, unique=True, verbose_name= ('Código'))
	#Creditos del curso
	creditos = models.PositiveIntegerField(verbose_name= ('Créditos'))
	#Horas del curso
	horas = models.PositiveIntegerField()
	#Horas de clase
	horasClase = models.PositiveIntegerField(verbose_name= ('Horas de Clase'), default=0)
	#Descripción del curso
	descripcion = models.CharField(max_length=200, verbose_name= ('Descripción'))
	#metodologia
	metodologia = models.CharField(max_length=5000, verbose_name= ('Descripción'))
	#tipo de curso
	tipo = models.ForeignKey(TipoAsignatura, on_delete=models.CASCADE,)
	#ciclo de curso
	ciclo = models.ForeignKey(CicloAsignatura, on_delete=models.CASCADE,)
	#escuela que lo gestiona
	escuela = models.ForeignKey(UnidadAcademica, on_delete=models.CASCADE, verbose_name= ('Unidad Académica'))
	#area
	area = models.ManyToManyField(Area, null=True, blank=True, verbose_name= ('Área') )


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Cursos"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.nombre

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.nombre

	def save(self, *args, **kwargs):
		self.horas = (self.creditos * 3) - self.horasClase
		super(Curso, self).save(*args, **kwargs)