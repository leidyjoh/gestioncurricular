# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from curso.models import Curso
from scc.models import SCC

#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class Componente(models.Model):
	"""Permite definir un listado de SCC"""

	#Número del componente
	numero = models.PositiveIntegerField(verbose_name= ('Número'))
	#Titulo del componente
	titulo = models.CharField(max_length=200, verbose_name= ('Título'))
	#Accion del componente
	accion = models.CharField(max_length=200, verbose_name= ('Acción'))
	#Contenido del componente
	contenido = models.CharField(max_length=200)
	#Contexto del componente
	contexto = models.CharField(max_length=200)
	#Finalidad del componente
	finalidad = models.CharField(max_length=200)
	#Descripción del componente
	descripcion = models.CharField(max_length=200, verbose_name= ('Descripción'))
	#Scc al que pertenece
	scc = models.ForeignKey(SCC, on_delete=models.CASCADE)
	#Programa al que pertenece
	#perfil = models.ForeignKey(Perfil, on_delete=models.CASCADE)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['titulo']
		verbose_name_plural = "Componentes del Programa"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.titulo

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.titulo


class MatrizTributacion(models.Model):
	"""Permite definir un listado de SCC"""

	#Id del curso
	curso = models.ForeignKey(Curso, on_delete=models.CASCADE)
	#Id del componente
	componente = models.ForeignKey(Componente, on_delete=models.CASCADE)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['curso']
		verbose_name_plural = "Matriz de Tributación"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.curso

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.curso