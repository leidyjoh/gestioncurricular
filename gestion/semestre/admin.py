# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import Semestre


"""Permite administrar la visualización de los datos de Perfil en la base de datos del
	sitio de administración"""
class AdminSemestre(admin.ModelAdmin):
	"""Permite establecer la información de los datos id y nombre del 
	modelo Perfil que se mostrarán en el sitio de administración"""
	list_display = ('id', 'numero', 'programa',)
	
	"""Permite establecer el parametro de busqueda nombre de la tabla Perfil en el sitio 
	de administración"""
	search_fields = ('id', 'programa')

#Permite registrar las clases Perfil y AdminPerfil
admin.site.register(Semestre, AdminSemestre)