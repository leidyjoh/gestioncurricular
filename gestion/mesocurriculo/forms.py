# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import TemplateView
from django.template import loader
from django.template import Context
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse
from django.contrib import messages
from django import forms
from inicio.mixins import LoginRequiredMixin
from scc.models import SCC
import json

"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""
class SCCCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""	


	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = SCC
		fields = ['titulo', 'descripcion']

"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class MesoCreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = SCCCreateForm
	model = SCC

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para crear un VehículoEspecial"""
		context = super(MesoCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nueva SCC'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"La scc ha sido creada con exito")
		return reverse_lazy('facultad:listar')


"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay 
errores muestra mensajes de error y si no, guarda el objeto"""	
class MesoCreateView(LoginRequiredMixin, TemplateView):
	"""Permite mostrar el formulario para crear un usuario"""

	def get(self,request,*args,**kwargs):
		"""Permite desplegar el formulario de creación de usuario y personal para un 
		peaje"""
		scc_form = SCCCreateForm()

		forms = [scc_form]
		context = {
		'section_title':'Crear SCC',
		'scc_form':scc_form,}

		return render(request, 'mesocurriculo/mesocurriculo_form.html', context)


	def post(self,request,*args,**kwargs):
		"""Permite enviar el formulario con la información del usuario, si es valido 
		guarda los datos de lo contrario redirecciona a la lista de usuarios"""

		scc_form = SCCCreateForm(request.POST)

		if scc_form.is_valid():
			scc_form.save()
			messages.info(request,'Nuevo scc creado con exito')
			url = reverse('facultad:listar')

			return HttpResponseRedirect(url)

		scc_form = SCCCreateForm(request.POST)
		messages.error(request,'Hay errores en algun campo, revise el formulario')

		context = {
		'section_title':'Crear SCC',
		'scc_form':scc_form,}

		return render(request, 'mesocurriculo/mesocurriculo_form.html', context)

"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay 
errores muestra mensajes de error y si no, guarda el objeto"""	
class MesoCreateTablaView(LoginRequiredMixin, TemplateView):
	"""Permite mostrar el formulario para crear un usuario"""

	def get(self,request,*args,**kwargs):
		"""Permite desplegar el formulario de creación de usuario y personal para un 
		peaje"""
		scc_form = SCCCreateForm()

		forms = [scc_form]
		context = {
		'section_title':'Crear SCC',
		'scc_form':scc_form,}

		return render(request, 'mesocurriculo/mesocurriculo_form_tabla.html', context)


	def post(self,request,*args,**kwargs):
		"""Permite enviar el formulario con la información del usuario, si es valido 
		guarda los datos de lo contrario redirecciona a la lista de usuarios"""

		scc_form = SCCCreateForm(request.POST)

		if scc_form.is_valid():
			scc_form.save()
			messages.info(request,'Nuevo scc creado con exito')
			url = reverse('facultad:listar')

			return HttpResponseRedirect(url)

		scc_form = SCCCreateForm(request.POST)
		messages.error(request,'Hay errores en algun campo, revise el formulario')

		context = {
		'section_title':'Crear SCC',
		'scc_form':scc_form,}

		return render(request, 'mesocurriculo/mesocurriculo_form_tabla.html', context)


"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay 
errores muestra mensajes de error y si no, guarda el objeto"""	
class MesoCreateArbolView(LoginRequiredMixin, TemplateView):
	"""Permite mostrar el formulario para crear un usuario"""

	def get(self,request,*args,**kwargs):
		"""Permite desplegar el formulario de creación de usuario y personal para un 
		peaje"""
		scc_form = SCCCreateForm()

		forms = [scc_form]
		context = {
		'section_title':'Crear SCC',
		'scc_form':scc_form,}

		return render(request, 'mesocurriculo/mesocurriculo_form_arbol.html', context)


	def post(self,request,*args,**kwargs):
		"""Permite enviar el formulario con la información del usuario, si es valido 
		guarda los datos de lo contrario redirecciona a la lista de usuarios"""

		scc_form = SCCCreateForm(request.POST)

		if scc_form.is_valid():
			scc_form.save()
			messages.info(request,'Nuevo scc creado con exito')
			url = reverse('facultad:listar')

			return HttpResponseRedirect(url)

		scc_form = SCCCreateForm(request.POST)
		messages.error(request,'Hay errores en algun campo, revise el formulario')

		context = {
		'section_title':'Crear SCC',
		'scc_form':scc_form,}

		return render(request, 'mesocurriculo/mesocurriculo_form_arbol.html', context)


"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay 
errores muestra mensajes de error y si no, guarda el objeto"""	
class MesoCreateListaView(LoginRequiredMixin, TemplateView):
	"""Permite mostrar el formulario para crear un usuario"""

	def get(self,request,*args,**kwargs):
		"""Permite desplegar el formulario de creación de usuario y personal para un 
		peaje"""
		scc_form = SCCCreateForm()

		forms = [scc_form]
		context = {
		'section_title':'Crear SCC',
		'scc_form':scc_form,}

		return render(request, 'mesocurriculo/mesocurriculo_form_lista.html', context)


	def post(self,request,*args,**kwargs):
		"""Permite enviar el formulario con la información del usuario, si es valido 
		guarda los datos de lo contrario redirecciona a la lista de usuarios"""

		scc_form = SCCCreateForm(request.POST)

		if scc_form.is_valid():
			scc_form.save()
			messages.info(request,'Nuevo scc creado con exito')
			url = reverse('facultad:listar')

			return HttpResponseRedirect(url)

		scc_form = SCCCreateForm(request.POST)
		messages.error(request,'Hay errores en algun campo, revise el formulario')

		context = {
		'section_title':'Crear SCC',
		'scc_form':scc_form,}

		return render(request, 'mesocurriculo/mesocurriculo_form_lista.html', context)