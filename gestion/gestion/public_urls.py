"""gestion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from gestion import settings
from django.views.static import serve
from django.conf.urls.static import static
from inicio import views
from django.conf.urls import handler404, handler500

urlbase = 'syllabus/'

urlpatterns = [
    path(urlbase + 'admin/', admin.site.urls),
    path(urlbase + 'admin/doc/', include('django.contrib.admindocs.urls')),
    path(urlbase + 'summernote/', include('django_summernote.urls')),
    #path(urlbase + 'searchableselect/', include('searchableselect.urls')),
    path(urlbase + '', include('inicio.urls', namespace='inicio')),
    path(urlbase + '', include('cuenta.urls', namespace='cuenta')),
    path(urlbase + '', include('usuario.urls', namespace='usuario')),
    path(urlbase + '', include('facultad.urls', namespace='facultad')),
    path(urlbase + '', include('unidadacademica.urls', namespace='unidadacademica')),
    path(urlbase + '', include('programa.urls', namespace='programa')),
    path(urlbase + '', include('semestre.urls', namespace='semestre')),
    path(urlbase + '', include('mallacurricular.urls', namespace='mallacurricular')),
    path(urlbase + '', include('matriztributacion.urls', namespace='matriztributacion')),
    path(urlbase + '', include('sccfacultad.urls', namespace='sccfacultad')),
    path(urlbase + '', include('componentefacultad.urls', namespace='componentefacultad')),
    path(urlbase + '', include('curso.urls', namespace='curso')),
    path(urlbase + '', include('perfil.urls', namespace='perfil')),
    path(urlbase + '', include('ra.urls', namespace='ra')),
    path(urlbase + '', include('diseñocurso.urls', namespace='diseñocurso')),
    path(urlbase + '', include('indicadorlogro.urls', namespace='indicadorlogro')),
    path(urlbase + '', include('actividadaprendizaje.urls', namespace='actividadaprendizaje')),
    path(urlbase + '', include('actividadevaluativa.urls', namespace='actividadevaluativa')),
    path(urlbase + '', include('mesocurriculo.urls', namespace='mesocurriculo')),
    path(urlbase + '', include('evaluacion.urls', namespace='evaluacion')),
    path(urlbase + 'chaining/', include('smart_selects.urls')),
    path(urlbase + 'social/', include('social.apps.django_app.urls', namespace='social')),

    path(urlbase + 'media/<path>', serve, {'document_root': settings.MEDIA_ROOT}),

    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = 'inicio.views.pag_404_not_found'
handler500 = 'inicio.views.pag_500_error_server'