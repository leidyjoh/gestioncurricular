# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import UnidadAcademica


"""Permite administrar la visualización de los datos de Unidad Academica en la base de datos del
	sitio de administración"""
class AdminUnidadAcademica(admin.ModelAdmin):
	"""Permite establecer la información de los datos id, nombre y facultad del 
	modelo UnidadAcademica que se mostrarán en el sitio de administración"""
	list_display = ('id', 'nombre', 'facultad')
	
	"""Permite establecer el parametro de busqueda nombre de la tabla UnidadAcademica en el sitio 
	de administración"""
	search_fields = ('nombre',)

#Permite registrar las clases UnidadAcademica y AdminUnidadAcademica
admin.site.register(UnidadAcademica, AdminUnidadAcademica)