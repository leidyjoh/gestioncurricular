# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from ra.models import ResultadoAprendizaje


#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class IndicadorLogro(models.Model):
	"""Permite definir un listado de SCC"""

	#Número del ro
	numero = models.PositiveIntegerField(verbose_name= ('Número'))
	#Accion del ra
	accion = models.CharField(max_length=200, verbose_name= ('Acción'), null=True, blank=True,)
	#Contenido del ra
	contenido = models.CharField(max_length=200, null=True, blank=True,)
	#Contexto del ra
	contexto = models.CharField(max_length=200, null=True, blank=True,)
	#Descripción del ra
	descripcion = models.TextField(max_length=250, verbose_name= ('Descripción'))
	#Componente al que pertenece
	resultadoAprendizaje = models.ForeignKey(ResultadoAprendizaje, on_delete=models.CASCADE, verbose_name= ('Resultado de Aprendizaje'))


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['numero']
		verbose_name_plural = "Indicadores de Logro"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return str(self.numero)

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.numero)
