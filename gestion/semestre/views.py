# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from .models import Semestre
from inicio.mixins import LoginRequiredMixin
from django.shortcuts import render


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class SemestreListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = Semestre
	context_object_name = 'semestres'
	template_name = 'semestre/semestre_list.html'

	def get_queryset(self):
		#persona = self.kwargs['pk']
		#unidad = user.unidadacademica
		if self.request.user.profesor.cargoAdministrativo == "Director de Escuela":
			return Semestre.objects.filter(programa__unidadacademica=self.request.user.profesor.unidadAcademica)
		elif self.request.user.profesor.cargoAdministrativo == "Director de Programa":
			return Semestre.objects.filter(programa__unidadacademica=self.request.user.profesor.unidadAcademica)