# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from facultad.models import Facultad


#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class SCCFacultad(models.Model):
	"""Permite definir un listado de SCC"""

	#Titulo de la scc
	numero = models.PositiveIntegerField(verbose_name= ('Número'))
	#titulo de la scc
	titulo = models.CharField(max_length=200, verbose_name= ('Título'))
	#Descripción de la scc
	descripcion = models.TextField(verbose_name= ('Descripción'))
	#facultad de scc
	facultad = models.ForeignKey(Facultad, on_delete=models.CASCADE)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['numero']
		verbose_name_plural = "SCCS de Facultad"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return str(self.numero) +". " + self.titulo

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return str(self.numero) + ". "+ self.titulo
