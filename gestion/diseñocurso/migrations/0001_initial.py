# Generated by Django 2.0.5 on 2021-06-10 06:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('mallacurricular', '0001_initial'),
        ('actividadevaluativa', '0001_initial'),
        ('actividadaprendizaje', '0001_initial'),
        ('usuario', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DiseñoActApRA',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('actividadAprendizaje', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='actividadaprendizaje.ActividadAprendizaje')),
            ],
            options={
                'verbose_name_plural': 'Diseños Cursos-ActividadAprendizaje-RA',
                'ordering': ['diseñoCurso'],
            },
        ),
        migrations.CreateModel(
            name='DiseñoActEvaRO',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('actividadEvaluacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='actividadE', to='actividadevaluativa.ActividadEvaluacion')),
            ],
            options={
                'verbose_name_plural': 'Diseños Cursos-ActividadEvaluacion-RO',
                'ordering': ['diseñoCurso'],
            },
        ),
        migrations.CreateModel(
            name='DiseñoCurso',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('curso', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Cursosdiseño', to='mallacurricular.CursosMalla')),
                ('docente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='usuario.Profesor')),
            ],
            options={
                'verbose_name_plural': 'Diseños Cursos',
                'ordering': ['curso'],
            },
        ),
        migrations.AddField(
            model_name='diseñoactevaro',
            name='diseñoCurso',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Dcurso', to='diseñocurso.DiseñoCurso'),
        ),
        migrations.AddField(
            model_name='diseñoactevaro',
            name='indicadorLogro',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='actividadevaluativa.ActividadEvaluacion'),
        ),
        migrations.AddField(
            model_name='diseñoactapra',
            name='diseñoCurso',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='diseñarcurso', to='diseñocurso.DiseñoCurso'),
        ),
    ]
