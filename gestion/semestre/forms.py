# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView
from django.template import loader
from django.template import Context
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse
from django.contrib import messages
from django import forms
from inicio.mixins import LoginRequiredMixin
from .models import Semestre
import json

"""FormView: Es una vista que muestra un formulario. En caso de error, vuelve a mostrar el 
formulario con errores de validación; en caso de éxito, redirige a una nueva URL"""
class SemestreCreateForm(LoginRequiredMixin, forms.ModelForm):
	"""Permite crear un formulario con los campos del facultad"""
	
	class Meta:
		"""Permite determinar del modelo Facultad y el campo nombre que se muestran en el formulario"""
		model = Semestre
		fields = ['numero', 'programa']

"""CreateView: Es una vista que muestra un formulario para crear un objeto. Si hay errores
	muestra mensajes de error y si no, guarda el objeto"""	
class SemestreCreateView(LoginRequiredMixin, CreateView):
	"""Permite mostrar el formulario para crear una facultad"""
	form_class = SemestreCreateForm
	model = Semestre

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para crear un VehículoEspecial"""
		context = super(SemestreCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nuevo Semestre'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El Semestre ha sido creado con exito")
		return reverse_lazy('semestre:listar')

"""UpdateView: Es una vista que muestra un formulario para editar un objeto existente, 
en caso de un error retorna el formulario, de lo contrario guarda los cambios. Esto 
utiliza un formulario generado automáticamente a partir de la clase de modelo del objeto"""
class SemestreUpdateView(LoginRequiredMixin, UpdateView):
	"""Permite actualizar la información de una facultad"""

	model = Semestre
	fields = ['numero', 'programa']
	success_url = reverse_lazy('semestre:listar')

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla 
		para actualizar una facultad"""
		context = super(SemestreUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Semestre'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		"""Permite mostrar un mensaje de confirmación y redirecciona al listado de 
		VehiculoEspecial"""
		messages.info(self.request,"El Semestre ha sido actualizado con exito")
		return reverse_lazy('semestre:listar')

class SemestreDeleteView(LoginRequiredMixin, DeleteView):
	"""Permite mientras se ejecuta contener el objeto persona sobre el que se esta 
	operando la vista"""
	
	model = Semestre
	template_name = 'semestre/semestre_confirm_delete.html'
	success_url = reverse_lazy('semestre:listar')