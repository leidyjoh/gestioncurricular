# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from .models import Profesor, Estudiante
from inicio.mixins import LoginRequiredMixin
from django.shortcuts import render


"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class DocenteListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = Profesor
	context_object_name = 'docentes'
	template_name = 'usuario/docente_list.html'

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		context = super(DocenteListView,self).get_context_data(**kwargs)
		return context

"""ListView: Es una vista que permite mostrar un listado de cualquier objeto existente"""
class EstudianteListView(LoginRequiredMixin, ListView):
	"""Permite listar todos los vehículos especiales de la base de datos"""

	model = Estudiante
	context_object_name = 'estudiantes'
	template_name = 'usuario/estudiante_list.html'

	def get_context_data(self,**kwargs):
		"""Permite devolver un diccionario que representa el contexto de la plantilla para 
			listar los vehículos especiales"""
		context = super(EstudianteListView,self).get_context_data(**kwargs)
		return context