# -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import ComponenteFacultadCreateView, ComponenteFacultadUpdateView
from .views import ComponenteFacultadListView

app_name = 'componentefacultad'

urlpatterns = [

	path('componentefacultad/crear/', ComponenteFacultadCreateView.as_view(), name='crear'),
	path('componentefacultad/listar/', ComponenteFacultadListView.as_view(), name='listar'),
	path('componentefacultad/actualizar/<int:pk>', ComponenteFacultadUpdateView.as_view(), name='actualizar'),
]