# -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import ActividadEvaluativaCreateView, ActividadEvaluativaUpdateView
from .views import ActividadEvaluativaListView

app_name = 'actividadevaluativa'

urlpatterns = [

	path('actividadevaluativa/crear/', ActividadEvaluativaCreateView.as_view(), name='crear'),
	path('actividadevaluativa/listar/', ActividadEvaluativaListView.as_view(), name='listar'),
	path('actividadevaluativa/actualizar/<int:pk>', ActividadEvaluativaUpdateView.as_view(), name='actualizar'),
]