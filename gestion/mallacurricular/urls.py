 # -*- coding: utf-8 -*-
from django.urls import path
from . import views
from .forms import MallaCreateView, MallaUpdateView
from .views import MallaListView, PdfMallaCreateView, PdfFormacionCreateView, PdfFormacionGeneralCreateView, CursosListView, ProgramasMallaListView, PdfTotalCreateView

app_name = 'mallacurricular'

urlpatterns = [

	path('mallacurricular/crear/(<int:epk>)', MallaCreateView.as_view(), name='crear'),
	path('mallacurricular/listar/', MallaListView.as_view(), name='listar'),
	path('mallacurricular/listarprogramas/', ProgramasMallaListView.as_view(), name='listarPro'),
	path('mallacurricular/cursosmalla/', CursosListView.as_view(), name='listarCursos'),
	path('mallacurricular/actualizar/<int:pk>', MallaUpdateView.as_view(), name='actualizar'),
	path('mallacurricular/ver/(<int:pk>)', PdfMallaCreateView.as_view(), name='mallacurricularpdf'),
	path('mallacurricular/FormacionPDF/(<int:pk>)', PdfFormacionCreateView.as_view(), name='formaciongpdf'),
	path('mallacurricular/FormacionGeneralPDF/(<int:pk>)', PdfFormacionGeneralCreateView.as_view(), name='formacionpdf'),
	path('mallacurricular/Totalizadores/(<int:pk>)', PdfTotalCreateView.as_view(), name='total'),
]