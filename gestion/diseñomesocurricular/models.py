# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from usuario.models import Directivo
from programa.models import Programa

tipo_estado = (
	(True, 'Activo'),
	(False, 'Inactivo'),
)

#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"
"""Model: Define la estructura de la tabla Programa en base de datos"""
class DiseñoMesocurricular(models.Model):
	"""Permite definir un listado de Diseños Mesocurriculares"""

	#Director de escuela que aprueba el diseño
	director = models.ForeignKey(Directivo, on_delete=models.CASCADE)
	#Programa
	Programa = models.ForeignKey(Programa, on_delete=models.CASCADE)
	#Fecha del diseño
	fecha = models.CharField(max_length=200)
	#Versión del diseño
	version = models.CharField(max_length=200)
	#Estado del diseño
	estado = models.BooleanField(choices=tipo_estado)


	"""Permite ordenar la lista de programas por nombre y asignarle el nombre en 
	plural"""
	class Meta:
		ordering = ['curso']
		verbose_name_plural = "Diseños Mesocurriculares"

	#Permite determinar una representación en string del objeto Programa
	def __str__(self):
		return self.version

	"""Permite determinar una representación en string para el objeto Programa (Para 
	versiones de Python 2)"""
	def __unicode__(self):
		return self.version